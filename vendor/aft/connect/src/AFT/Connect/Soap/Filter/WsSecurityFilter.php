<?php

namespace AFT\Connect\Soap\Filter;

use ass\XmlSecurity\Key;
use BeSimple\SoapClient\WsSecurityFilter as BaseWsSecurityFilter;
use BeSimple\SoapCommon\FilterHelper;
use BeSimple\SoapCommon\Helper;
use BeSimple\SoapCommon\SoapRequest;
use BeSimple\SoapCommon\WsSecurityKey;

class WsSecurityFilter extends BaseWsSecurityFilter
{
    public function __construct()
    {
        parent::__construct(false, null);

        $this->setSecurityOptionsSignature(self::TOKEN_REFERENCE_SECURITY_TOKEN);
        $this->setSecurityOptionsEncryption(self::TOKEN_REFERENCE_THUMBPRINT_SHA1);

        $this->setUserSecurityKeyObject(new WsSecurityKey());
        $this->setServiceSecurityKeyObject(new WsSecurityKey());
        $this->serviceSecurityKey->addPrivateKey(Key::TRIPLEDES_CBC);
    }

    public function setUserPublicKey($key, $isFile = true)
    {
        $this->userSecurityKey->addPublicKey(Key::RSA_SHA1, $key, $isFile);
    }

    public function setUserPrivateKey($key, $isFile = true, $passphrase = null)
    {
        $this->userSecurityKey->addPrivateKey(Key::RSA_SHA1, $key, $isFile, $passphrase);
    }

    public function setServicePublicKey($key, $isFile = true)
    {
        $this->serviceSecurityKey->addPublicKey(Key::RSA_1_5, $key, $isFile);
    }

    public function filterRequest(SoapRequest $request)
    {
        parent::filterRequest($request);

        if (null === $this->password) {
            return;
        }

        // get \DOMDocument from SOAP request
        $dom = $request->getContentDocument();

        $usernameToken = $dom->getElementsByTagNameNs(Helper::NS_WSS, 'UsernameToken');
        if (!$usernameToken->length) {
            return;
        }

        // create FilterHelper
        $filterHelper = new FilterHelper($dom);

        // add the neccessary namespaces
        $filterHelper->addNamespace(Helper::PFX_WSS, Helper::NS_WSS);
        $filterHelper->addNamespace(Helper::PFX_WSU, Helper::NS_WSU);

        $dt = new \DateTime('now', new \DateTimeZone('UTC'));
        $createdTimestamp = $dt->format(self::DATETIME_FORMAT);

        $usernameToken = $usernameToken->item(0);
        if (self::PASSWORD_TYPE_DIGEST === $this->passwordType) {
            $nonce = mt_rand();
            $password = base64_encode(sha1($nonce.$createdTimestamp.$this->password, true));
            $passwordType = Helper::NAME_WSS_UTP.'#PasswordDigest';
        } else {
            $password = $this->password;
            $passwordType = Helper::NAME_WSS_UTP.'#PasswordText';
        }

        $password = $filterHelper->createElement(Helper::NS_WSS, 'Password', $password);
        $filterHelper->setAttribute($password, null, 'Type', $passwordType);
        $usernameToken->appendChild($password);

        if (self::PASSWORD_TYPE_DIGEST === $this->passwordType) {
            $nonce = $filterHelper->createElement(Helper::NS_WSS, 'Nonce', base64_encode($nonce));
            $usernameToken->appendChild($nonce);

            $created = $filterHelper->createElement(Helper::NS_WSU, 'Created', $createdTimestamp);
            $usernameToken->appendChild($created);
        }
    }

    protected function createNodeListForSigning(\DOMDocument $dom, \DOMElement $security)
    {
        $nodes = parent::createNodeListForSigning($dom, $security);

        foreach ($nodes as $i => $node) {
            if ($security === $node->parentNode) {
                unset($nodes[$i]);
            }
        }

        return $nodes;
    }
}
