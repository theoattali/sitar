<?php

namespace AFT\Connect\Api;

class UserToken
{
    protected $username;

    protected $token;

    protected $roles;

    protected $user;

    public function __construct($username, Token $token = null)
    {
        $this->username = $username;
        $this->token = $token;
        $this->roles = array();
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getToken()
    {
        return (string) $this->token;
    }

    public function getRoles()
    {
        if ($this->roles instanceof \stdClass) {
            $roles = array();

            if (isset($this->roles->item)) {
                foreach ((array) $this->roles->item as $role) {
                    $roles[] = $role;
                }
            }

            $this->roles = $roles;
        }

        return $this->roles;
    }

    public function getUser()
    {
        return $this->user;
    }
}
