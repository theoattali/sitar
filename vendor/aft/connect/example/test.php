<?php

use AFT\Connect\Soap\Filter\WsSecurityFilter;
use BeSimple\SoapClient\SoapClientBuilder;
use BeSimple\SoapCommon\Cache;

require __DIR__.'/../vendor/autoload.php';

Cache::setDirectory(__DIR__.'/cache');

$client = (new SoapClientBuilder())
    ->withWsdl('http://localhost/security/web/app_dev.php/soap/Security?wsdl')
    ->build()
;

$wssFilter = new WsSecurityFilter(false, null);
$wssFilter->addUserData('pegase', 'p3g4s3');

$wssFilter->setUserPublicKey(__DIR__.'/cert/pegasecert.pem');
$wssFilter->setUserPrivateKey(__DIR__.'/cert/pegasekey.pem');

$wssFilter->setServicePublicKey(__DIR__.'/cert/servercert.pem');

$client->getSoapKernel()->registerFilter($wssFilter);

var_dump($client->login(array(
    'username' => 'user',
    'password' => 'user'
)));
