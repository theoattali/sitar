<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year', Type\NumberType::class, array(
                'label' => 'label.billing.year',
                'attr' => array(
                    'placeholder' => 'label.billing.year',
                    'class' => 'form-control',
                ),
            ))
            ->add('domain', Type\TextType::class, array(
                'label' => 'label.billing.domain',
                'attr' => array(
                    'placeholder' => 'label.billing.domain',
                    'class' => 'form-control',
                ),
            ))
            ->add('team', Type\TextType::class, array(
                'label' => 'label.billing.team',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.billing.team',
                    'class' => 'form-control',
                ),
            ))
            ->add('teamId', Type\TextType::class, array(
                'label' => 'label.billing.teamId',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.billing.teamId',
                    'class' => 'form-control',
                ),
            ))
            ->add('mention', Type\TextType::class, array(
                'label' => 'label.billing.mention',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.billing.mention',
                    'class' => 'form-control',
                ),
            ))
            ->add('mode', Type\TextType::class, array(
                'label' => 'label.billing.mode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.billing.mode',
                    'class' => 'form-control',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Billing',
            'translation_domain' => 'AFTSiteBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_billing';
    }
}
