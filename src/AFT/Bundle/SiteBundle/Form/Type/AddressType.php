<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('streetNumber', Type\TextType::class, array(
                'label' => 'label.address.streetNumber',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.address.streetNumber',
                    'class' => 'form-control',
                ),
            ))
            ->add('streetLine1', Type\TextType::class, array(
                'label' => 'label.address.streetLine1',
                'attr' => array(
                    'placeholder' => 'label.address.streetLine1',
                    'class' => 'form-control',
                ),
            ))
            ->add('streetLine2', Type\TextType::class, array(
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.address.streetLine2',
                    'class' => 'form-control',
                ),
            ))
            ->add('streetLine3', Type\TextType::class, array(
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.address.streetLine3',
                    'class' => 'form-control',
                ),
            ))
            ->add('streetLine4', Type\TextType::class, array(
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.address.streetLine4',
                    'class' => 'form-control',
                ),
            ))
            ->add('zipCode', Type\TextType::class, array(
                'label' => 'label.address.zipCode',
                'attr' => array(
                    'placeholder' => 'label.address.zipCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('cityCode', Type\TextType::class, array(
                'label' => 'label.address.cityCode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.address.cityCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('cityName', Type\TextType::class, array(
                'label' => 'label.address.cityName',
                'attr' => array(
                    'placeholder' => 'label.address.cityName',
                    'class' => 'form-control',
                ),
            ))
            ->add('departement', EntityType::class, array(
                'label' => 'label.address.departement',
                'placeholder' => 'placeholder.address.departement',
                'choice_label' => 'label',
                'class' => 'AFTSiteBundle:Departement',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->orderBy('d.label', 'ASC');
                },
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('country', Type\CountryType::class, array(
                'label' => 'label.address.country',
                'placeholder' => 'placeholder.address.country',
                'preferred_choices' => array('FR'),
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('phoneCode', Type\TextType::class, array(
                'label' => 'label.address.phoneCode',
                'attr' => array(
                    'placeholder' => 'placeholder.address.phoneCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('phoneNumber', Type\TextType::class, array(
                'label' => 'label.address.phoneNumber',
                'attr' => array(
                    'placeholder' => 'placeholder.address.phoneNumber',
                    'class' => 'form-control',
                ),
            ))
            ->add('faxNumber', Type\TextType::class, array(
                'label' => 'label.address.faxNumber',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'placeholder.address.phoneNumber',
                    'class' => 'form-control',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Address',
            'translation_domain' => 'AFTSiteBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_address';
    }
}
