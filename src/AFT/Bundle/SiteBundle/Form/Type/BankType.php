<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('branchCode', Type\TextType::class, array(
                'label' => 'label.bank.branchCode',
                'attr' => array(
                    'placeholder' => 'label.bank.branchCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('bankName', Type\TextType::class, array(
                'label' => 'label.bank.bankName',
                'attr' => array(
                    'placeholder' => 'label.bank.bankName',
                    'class' => 'form-control',
                ),
            ))
            ->add('sortCode', Type\TextType::class, array(
                'label' => 'label.bank.sortCode',
                'attr' => array(
                    'placeholder' => 'label.bank.sortCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('accountNumber', Type\TextType::class, array(
                'label' => 'label.bank.accountNumber',
                'attr' => array(
                    'placeholder' => 'label.bank.accountNumber',
                    'class' => 'form-control',
                ),
            ))
            ->add('ribKey', Type\TextType::class, array(
                'label' => 'label.bank.ribKey',
                'attr' => array(
                    'placeholder' => 'label.bank.ribKey',
                    'class' => 'form-control',
                ),
            ))
            ->add('iban', Type\TextType::class, array(
                'label' => 'label.bank.iban',
                'attr' => array(
                    'placeholder' => 'label.bank.iban',
                    'class' => 'form-control',
                ),
            ))
            ->add('tvaIntracomNumber', Type\TextType::class, array(
                'label' => 'label.bank.tvaIntracomNumber',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.bank.tvaIntracomNumber',
                    'class' => 'form-control',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Bank',
            'translation_domain' => 'AFTSiteBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_bank';
    }
}
