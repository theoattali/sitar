<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', Type\TextType::class, array(
                'label' => 'label.site.code',
                'attr' => array(
                    'placeholder' => 'label.site.code',
                    'class' => 'form-control',
                ),
            ))
            ->add('corporateName', Type\TextType::class, array(
                'label' => 'label.site.corporateName',
                'attr' => array(
                    'placeholder' => 'label.site.corporateName',
                    'class' => 'form-control',
                ),
            ))
            ->add('name', Type\TextType::class, array(
                'label' => 'label.site.name',
                'attr' => array(
                    'placeholder' => 'label.site.name',
                    'class' => 'form-control',
                ),
            ))
            ->add('locality', Type\TextType::class, array(
                'label' => 'label.site.locality',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.site.locality',
                    'class' => 'form-control',
                ),
            ))
            ->add('websiteUrl', Type\TextType::class, array(
                'label' => 'label.site.websiteUrl',
                'attr' => array(
                    'value' => 'http://www.aftral.com',
                    'class' => 'form-control',
                ),
            ))
            ->add('mail', Type\TextType::class, array(
                'label' => 'label.site.mail',
                'attr' => array(
                    'placeholder' => 'label.site.mail',
                    'class' => 'form-control',
                ),
            ))
            ->add('openingTime', Type\TextType::class, array(
                'label' => 'label.site.openingTime',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.site.openingTime',
                    'class' => 'form-control',
                ),
            ))
            ->add('beginingActivityAt', Type\DateType::class, array(
                'label' => 'label.site.beginingActivityAt',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'placeholder' => 'label.site.beginingActivityAt',
                    'class' => 'form-control datePickerBegin',
                ),
            ))
            ->add('type', EntityType::class, array(
                'label' => 'label.site.type',
                'placeholder' => 'placeholder.site.type',
                'choice_label' => 'label',
                'class' => 'AFTSiteBundle:Type',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.label', 'ASC');
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'label.site.parent',
                'placeholder' => 'placeholder.site.site',
                'required' => false,
                'choice_label' => 'corporateName',
                'class' => 'AFTSiteBundle:Site',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.corporateName', 'ASC');
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('supervisor', EntityType::class, array(
                'label' => 'label.site.supervisor',
                'placeholder' => 'placeholder.site.supervisor',
                'required' => false,
                'choice_label' => function ($supervisor) {
                    return $supervisor->getLastname().' '.$supervisor->getFirstname();
                },
                'class' => 'AFTSiteBundle:Supervisor',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.lastname', 'ASC');
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('subjectTva', Type\CheckboxType::class, array(
                'label' => 'label.site.subjectTva',
                'required' => false,
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'button.save',
                'attr' => array(
                    'class' => 'btn btn-success btn-lg',
                ),
            ))
        ;

        if ($options['extended_form']) {
            $builder
                ->add('domain', Type\TextType::class, array(
                    'label' => 'label.site.domain',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'label.site.domain',
                        'class' => 'form-control',
                    ),
                ))
                ->add('numSiren', Type\TextType::class, array(
                    'label' => 'label.site.numSiren',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'label.site.numSiren',
                        'class' => 'form-control fillSiret',
                    ),
                ))
                ->add('numIc', Type\TextType::class, array(
                    'label' => 'label.site.numIc',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'label.site.numIc',
                        'class' => 'form-control fillSiret',
                    ),
                ))
                ->add('codeApeNaf', Type\TextType::class, array(
                    'label' => 'label.site.codeApeNaf',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'label.site.codeApeNaf',
                        'class' => 'form-control',
                    ),
                ))
                ->add('activityNumber', Type\TextType::class, array(
                    'label' => 'label.site.activityNumber',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'label.site.activityNumber',
                        'class' => 'form-control',
                    ),
                ))
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Site',
            'translation_domain' => 'AFTSiteBundle',
            'extended_form' => false,
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_site';
    }
}
