<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', Type\TextType::class, array(
                'label' => 'label.site.code',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.site.code',
                    'class' => 'form-control',
                ),
            ))
            ->add('corporateName', Type\TextType::class, array(
                'label' => 'label.site.corporateName',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.site.corporateName',
                    'class' => 'form-control',
                ),
            ))
            ->add('name', Type\TextType::class, array(
                'label' => 'label.site.name',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.site.name',
                    'class' => 'form-control',
                ),
            ))
            ->add('type', EntityType::class, array(
                'label' => 'label.site.type',
                'choice_label' => 'label',
                'placeholder' => 'placeholder.site.type',
                'class' => 'AFTSiteBundle:Type',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.label', 'ASC');
                },
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'button.search',
                'attr' => array(
                    'class' => 'btn btn-primary',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Site',
            'translation_domain' => 'AFTSiteBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_site_search';
    }
}
