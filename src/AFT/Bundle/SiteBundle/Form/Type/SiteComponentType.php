<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteComponentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ('address' === $options['component']) {
            $builder
                ->add('addresses', Type\CollectionType::class, array(
                    'entry_type' => AddressType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'error_bubbling' => false,
                ))
            ;

        } elseif ('bank' === $options['component']) {
            $builder
                ->add('bank', BankType::class)
            ;

        } elseif ('billing' === $options['component']) {
            $builder
                ->add('billing', BillingType::class)
            ;

        } else {
            $builder
                ->add('property', PropertyType::class)
            ;
        }

        $builder
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'button.save',
                'attr' => array(
                    'class' => 'btn btn-success btn-lg',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Site',
            'translation_domain' => 'AFTSiteBundle',
            'component' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_site_comp';
    }
}
