<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adonixCode', Type\TextType::class, array(
                'label' => 'label.property.adonixCode',
                'attr' => array(
                    'placeholder' => 'label.property.adonixCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('territoryCode', Type\TextType::class, array(
                'label' => 'label.property.territoryCode',
                'attr' => array(
                    'placeholder' => 'label.property.territoryCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('selligentCode', Type\TextType::class, array(
                'label' => 'label.property.selligentCode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.selligentCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('aftManagementCode', Type\TextType::class, array(
                'label' => 'label.property.aftManagementCode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.aftManagementCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('iftimManagementCode', Type\TextType::class, array(
                'label' => 'label.property.iftimManagementCode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.iftimManagementCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('aftralManagementCode', Type\TextType::class, array(
                'label' => 'label.property.aftralManagementCode',
                'attr' => array(
                    'placeholder' => 'label.property.aftralManagementCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('transportCode', Type\TextType::class, array(
                'label' => 'label.property.transportCode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.transportCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('logisticCode', Type\TextType::class, array(
                'label' => 'label.property.logisticCode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.logisticCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('backgroundColor', Type\TextType::class, array(
                'label' => 'label.property.backgroundColor',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.backgroundColor',
                    'class' => 'form-control',
                ),
            ))
            ->add('textColor', Type\TextType::class, array(
                'label' => 'label.property.textColor',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.textColor',
                    'class' => 'form-control',
                ),
            ))
            ->add('cqcNumber', Type\TextType::class, array(
                'label' => 'label.property.cqcNumber',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.cqcNumber',
                    'class' => 'form-control',
                ),
            ))
            ->add('cadrNumber', Type\TextType::class, array(
                'label' => 'label.property.cadrNumber',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.cadrNumber',
                    'class' => 'form-control',
                ),
            ))
            ->add('adCode', Type\TextType::class, array(
                'label' => 'label.property.adCode',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.adCode',
                    'class' => 'form-control',
                ),
            ))
            ->add('serverIp', Type\TextType::class, array(
                'label' => 'label.property.serverIp',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.serverIp',
                    'class' => 'form-control',
                ),
            ))
            ->add('serverName', Type\TextType::class, array(
                'label' => 'label.property.serverName',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.serverName',
                    'class' => 'form-control',
                ),
            ))
            ->add('virtualNdv', Type\CheckboxType::class, array(
                'label' => 'label.property.virtualNdv',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'label.property.virtualNdv',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Property',
            'translation_domain' => 'AFTSiteBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_property';
    }
}
