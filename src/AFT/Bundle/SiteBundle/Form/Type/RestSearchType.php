<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RestSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', Type\TextType::class, array(
                'error_bubbling' => true,
            ))
            ->add('corporateName', Type\TextType::class, array(
                'error_bubbling' => true,
            ))
            ->add('name', Type\TextType::class, array(
                'error_bubbling' => true,
            ))
            ->add('type',  Type\TextType::class, array(
                'error_bubbling' => true,
            ))
            ->add('limit', Type\TextType::class, array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Domain\RestSearchSiteDomain',
            'csrf_protection' => false,
        ));
    }

    public function getBlockPrefix()
    {
        return null;
    }
}
