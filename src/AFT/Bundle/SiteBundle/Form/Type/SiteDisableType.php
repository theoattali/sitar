<?php

namespace AFT\Bundle\SiteBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteDisableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('successor', EntityType::class, array(
                'label' => 'label.site.successor',
                'placeholder' => 'placeholder.site.site',
                'choice_label' => 'corporateName',
                'class' => 'AFTSiteBundle:Site',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.corporateName', 'ASC');
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('disabledAt', Type\DateType::class, array(
                'label' => 'label.site.disabledAt',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'placeholder' => 'placeholder.site.disabledAt',
                    'class' => 'form-control datePickerEnd',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AFT\Bundle\SiteBundle\Entity\Site',
            'translation_domain' => 'AFTSiteBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'aft_site_disable';
    }
}
