<?php

namespace AFT\Bundle\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AFT\Bundle\SiteBundle\Entity\Supervisor;

class LoadSupervisorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'supervisor_1' => array(
                'title' => null,
                'lastname' => 'APARICI',
                'firstname' => 'Vincent',
                'mail' => 'vincent.aparici@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_2' => array(
                'title' => null,
                'lastname' => 'ASSORIN',
                'firstname' => 'Corinne',
                'mail' => 'corinne.assorin@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_3' => array(
                'title' => null,
                'lastname' => 'AUGEREAU',
                'firstname' => 'Nathalie',
                'mail' => 'nathalie.augereau@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_4' => array(
                'title' => null,
                'lastname' => 'BASTIEN',
                'firstname' => 'Philippe',
                'mail' => 'philippe.bastien@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_5' => array(
                'title' => null,
                'lastname' => 'BASTIEN',
                'firstname' => 'Régine',
                'mail' => 'regine.bastien@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_6' => array(
                'title' => null,
                'lastname' => 'BEAUFORT',
                'firstname' => 'Gilles',
                'mail' => 'gilles.beaufort@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_7' => array(
                'title' => null,
                'lastname' => 'BEGUE',
                'firstname' => 'Jean-Claude',
                'mail' => 'jean-claude.begue@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_8' => array(
                'title' => null,
                'lastname' => 'BEJOINT',
                'firstname' => 'Franck',
                'mail' => 'franck.bejoint@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_9' => array(
                'title' => null,
                'lastname' => 'BIDART',
                'firstname' => 'Jerome',
                'mail' => 'jerome.bidart@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_10' => array(
                'title' => null,
                'lastname' => 'BIRBA',
                'firstname' => 'Sophie',
                'mail' => 'sophie.birba@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_11' => array(
                'title' => null,
                'lastname' => 'BOUSQUET',
                'firstname' => 'Jérôme',
                'mail' => 'jerome.bousquet@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_12' => array(
                'title' => null,
                'lastname' => 'BUFFET',
                'firstname' => 'Valérie',
                'mail' => 'valerie.buffet@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_13' => array(
                'title' => null,
                'lastname' => 'CHARPENTIER',
                'firstname' => 'Gérard',
                'mail' => 'gerard.charpentier@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_14' => array(
                'title' => null,
                'lastname' => 'CHASTAN',
                'firstname' => 'Pierre',
                'mail' => 'pierre.chastan@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_15' => array(
                'title' => null,
                'lastname' => 'CHRISTEN',
                'firstname' => 'Ghislaine',
                'mail' => 'ghislaine.christen@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_16' => array(
                'title' => null,
                'lastname' => 'CIREFICE',
                'firstname' => 'Jacques',
                'mail' => 'jacques.cirefice@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_17' => array(
                'title' => null,
                'lastname' => 'COIN',
                'firstname' => 'Christopher',
                'mail' => 'christopher.coin@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_18' => array(
                'title' => null,
                'lastname' => 'COPPA',
                'firstname' => 'Laurent',
                'mail' => 'laurent.coppa@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_19' => array(
                'title' => null,
                'lastname' => 'CROIZON',
                'firstname' => 'Bruno',
                'mail' => 'bruno.croizon@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_20' => array(
                'title' => null,
                'lastname' => 'DE SURONE',
                'firstname' => 'Pierre',
                'mail' => 'pierre.desurone@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_21' => array(
                'title' => null,
                'lastname' => 'DENEAU ROUFFIGNAC',
                'firstname' => 'Muriel',
                'mail' => 'muriel.deneau@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_22' => array(
                'title' => null,
                'lastname' => 'DESCHAMPS',
                'firstname' => 'Rémy',
                'mail' => 'remy.deschamps@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_23' => array(
                'title' => null,
                'lastname' => 'DESSAIVRE',
                'firstname' => 'Damien',
                'mail' => 'damien.dessaivre@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_24' => array(
                'title' => null,
                'lastname' => 'DEVAUX',
                'firstname' => 'Jean-Denis',
                'mail' => 'jean-denis.devaux@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_25' => array(
                'title' => null,
                'lastname' => 'DUHAUTBOUT',
                'firstname' => 'Dominique',
                'mail' => 'dominique.duhautbout@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_26' => array(
                'title' => null,
                'lastname' => 'DURAND',
                'firstname' => 'Didier',
                'mail' => 'didier.durand@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_27' => array(
                'title' => null,
                'lastname' => 'FILLAUDEAU',
                'firstname' => 'Franck',
                'mail' => 'franck.fillaudeau@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_28' => array(
                'title' => null,
                'lastname' => 'FOURNIER',
                'firstname' => 'Christine',
                'mail' => 'christine.fournier@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_29' => array(
                'title' => null,
                'lastname' => 'FRANCOLIN',
                'firstname' => 'Sandrine',
                'mail' => 'sandrine.francolin@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_30' => array(
                'title' => null,
                'lastname' => 'GARCIA',
                'firstname' => 'Régis',
                'mail' => 'regis.garcia@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_31' => array(
                'title' => null,
                'lastname' => 'GARNIER',
                'firstname' => 'Christophe',
                'mail' => 'christophe.garnier@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_32' => array(
                'title' => null,
                'lastname' => 'GOUILLY-FORTIN',
                'firstname' => 'Arnaud',
                'mail' => 'arnaud.gouilly-fortin@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_33' => array(
                'title' => null,
                'lastname' => 'GRANDREMY',
                'firstname' => 'Fabien',
                'mail' => 'fabien.grandremy@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_34' => array(
                'title' => null,
                'lastname' => 'GUERRA',
                'firstname' => 'Sophie',
                'mail' => 'sophie.guerra@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_35' => array(
                'title' => null,
                'lastname' => 'HAYERE',
                'firstname' => 'Olivia',
                'mail' => 'olivia.hayere@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_36' => array(
                'title' => null,
                'lastname' => 'HERVE',
                'firstname' => 'Delphine',
                'mail' => 'delphine.herve@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_37' => array(
                'title' => null,
                'lastname' => 'JACQUEMET',
                'firstname' => 'Sophie',
                'mail' => 'sophie.jacquemet@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_38' => array(
                'title' => null,
                'lastname' => 'JOUHANET',
                'firstname' => 'Christophe',
                'mail' => 'christophe.jouhanet@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_39' => array(
                'title' => null,
                'lastname' => 'KLAUS HUMBERT',
                'firstname' => 'Jessica',
                'mail' => 'jessica.klaus-hombert@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_40' => array(
                'title' => null,
                'lastname' => 'LAFFITTE',
                'firstname' => 'Yann',
                'mail' => 'yann.laffitte@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_41' => array(
                'title' => null,
                'lastname' => 'LAFITTE',
                'firstname' => 'Yann',
                'mail' => 'yann.laffitte@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_42' => array(
                'title' => null,
                'lastname' => 'LANSIGU',
                'firstname' => 'Elisabeth',
                'mail' => 'elisabeth.lansigu@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_43' => array(
                'title' => null,
                'lastname' => 'LEGRAND',
                'firstname' => 'Thierry',
                'mail' => 'thierry.legrand@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_44' => array(
                'title' => null,
                'lastname' => 'LETURGEON',
                'firstname' => 'Benoit',
                'mail' => 'benoit.leturgeon@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_45' => array(
                'title' => null,
                'lastname' => 'MAGGIONI',
                'firstname' => 'Philippe',
                'mail' => 'philippe.maggioni@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_46' => array(
                'title' => null,
                'lastname' => 'MANTEAU',
                'firstname' => 'Méline',
                'mail' => 'meline.manteau@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_47' => array(
                'title' => null,
                'lastname' => 'MARTHOU',
                'firstname' => 'Yvan',
                'mail' => 'yvan.marthou@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_48' => array(
                'title' => null,
                'lastname' => 'MOUBARAK',
                'firstname' => 'Gwenaëlle',
                'mail' => 'gwenaelle.moubarak@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_49' => array(
                'title' => null,
                'lastname' => 'NEDELLEC',
                'firstname' => 'Véronique',
                'mail' => 'veronique.nedellec@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_50' => array(
                'title' => null,
                'lastname' => 'PAILLERET',
                'firstname' => 'Gérald',
                'mail' => 'gerald.pailleret@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_51' => array(
                'title' => null,
                'lastname' => 'PENEAU',
                'firstname' => 'Jean-Michel',
                'mail' => 'jean-michel.peneau@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_52' => array(
                'title' => null,
                'lastname' => 'PICHAVANT',
                'firstname' => 'Jean-Pierre',
                'mail' => 'jean-pierre.pichavant@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_53' => array(
                'title' => null,
                'lastname' => 'POSMYK',
                'firstname' => 'Alain',
                'mail' => 'alain.posmyk@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_54' => array(
                'title' => null,
                'lastname' => 'POUPARD',
                'firstname' => 'Eric',
                'mail' => 'eric.poupard@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_55' => array(
                'title' => null,
                'lastname' => 'RABUSSIER',
                'firstname' => 'Hervé',
                'mail' => 'herve.rabussier@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_56' => array(
                'title' => null,
                'lastname' => 'RADOUAN',
                'firstname' => 'Joël',
                'mail' => 'joel.radouan@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_57' => array(
                'title' => null,
                'lastname' => 'RAITIERE',
                'firstname' => 'Stéphane',
                'mail' => 'stephane.raitiere@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_58' => array(
                'title' => null,
                'lastname' => 'RAJUSTEL',
                'firstname' => 'Frédéric',
                'mail' => 'frederic.rajustel@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_59' => array(
                'title' => null,
                'lastname' => 'RECORD',
                'firstname' => 'Denis',
                'mail' => 'denis.record@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_60' => array(
                'title' => null,
                'lastname' => 'RECROSIO',
                'firstname' => 'Jean-Louis',
                'mail' => 'jean-louis.recrosio@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_61' => array(
                'title' => null,
                'lastname' => 'REY',
                'firstname' => 'Jean-Louis',
                'mail' => 'jean-louis.rey@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_62' => array(
                'title' => null,
                'lastname' => 'ROCHE',
                'firstname' => 'Gilles',
                'mail' => 'gilles.roche@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_63' => array(
                'title' => null,
                'lastname' => 'SAMAMA',
                'firstname' => 'Joseph',
                'mail' => 'joseph-claude.samama@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_64' => array(
                'title' => null,
                'lastname' => 'SANCHEZ',
                'firstname' => 'Sylvain',
                'mail' => 'sylvain.sanchez@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_65' => array(
                'title' => null,
                'lastname' => 'THOMASSET',
                'firstname' => 'Maryline',
                'mail' => 'maryline.thomasset@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_66' => array(
                'title' => null,
                'lastname' => 'TRAVERS',
                'firstname' => 'Sylvie',
                'mail' => 'sylvie.travers@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_67' => array(
                'title' => null,
                'lastname' => 'TRUDELLE',
                'firstname' => 'Sandrine',
                'mail' => 'sandrine.trudelle@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_68' => array(
                'title' => null,
                'lastname' => 'VANDAMME',
                'firstname' => 'Thierry',
                'mail' => 'thierry.vandamme@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_69' => array(
                'title' => null,
                'lastname' => 'VARRE',
                'firstname' => 'Thomas',
                'mail' => 'thomas.varre@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_70' => array(
                'title' => null,
                'lastname' => 'VERGER',
                'firstname' => 'Marc',
                'mail' => 'marc.verger@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_71' => array(
                'title' => null,
                'lastname' => 'VIBRAC',
                'firstname' => 'Pierre',
                'mail' => 'pierre.vibrac@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_72' => array(
                'title' => null,
                'lastname' => 'VUITTON',
                'firstname' => 'Bruno',
                'mail' => 'bruno.vuitton@aftral.com',
                'role' => 'role_2',
            ),
            'supervisor_73' => array(
                'title' => null,
                'lastname' => 'WCISLO',
                'firstname' => 'Christelle',
                'mail' => 'christelle.wcislo@aftral.com',
                'role' => 'role_3',
            ),
            'supervisor_74' => array(
                'title' => null,
                'lastname' => 'WEILLAERT',
                'firstname' => 'Fabrice',
                'mail' => 'fabrice.weillaert@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_75' => array(
                'title' => null,
                'lastname' => 'YVON',
                'firstname' => 'David',
                'mail' => 'david.yvon@aftral.com',
                'role' => 'role_4',
            ),
            'supervisor_76' => array(
                'title' => null,
                'lastname' => 'CHARBONNIER',
                'firstname' => 'Loïc',
                'mail' => 'loic.charbonnier@aftral.com',
                'role' => 'role_1',
            ),
        );

        foreach ($objects as $key => $object) {
            $supervisor = new Supervisor();
            $supervisor->setTitle($object['title']);
            $supervisor->setLastname($object['lastname']);
            $supervisor->setFirstname($object['firstname']);
            $supervisor->setMail($object['mail']);

            if (isset($object['role'])) {
                $supervisor->setRole($this->getReference($object['role']));
            }

            $manager->persist($supervisor);
            $this->addReference($key, $supervisor);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
