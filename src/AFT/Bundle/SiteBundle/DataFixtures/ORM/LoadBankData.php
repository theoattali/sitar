<?php

namespace AFT\Bundle\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AFT\Bundle\SiteBundle\Entity\Bank;

class LoadBankData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'bank_1' => array(
                'branchCode' => '30066',
                'bankName' => 'CIC PARIS PRONY',
                'sortCode' => '10141',
                'accountNumber' => '00010310201',
                'ribKey' => '27',
                'iban' => 'FR76 3006 6101 4100 0103 1020 127',
                'tvaIntracomNumber' => 'FR95305405045',
                'site' => 'site_7',
            ),
            'bank_2' => array(
                'branchCode' => '30066',
                'bankName' => 'CIC PARIS PRONY',
                'sortCode' => '10141',
                'accountNumber' => '00010310201',
                'ribKey' => '27',
                'iban' => 'FR76 3006 6101 4100 0103 1020 127',
                'tvaIntracomNumber' => 'FR95305405045',
                'site' => 'site_32',
            ),
            'bank_3' => array(
                'branchCode' => '30066',
                'bankName' => 'CIC PARIS PRONY',
                'sortCode' => '10141',
                'accountNumber' => '00010310201',
                'ribKey' => '27',
                'iban' => 'FR76 3006 6101 4100 0103 1020 127',
                'tvaIntracomNumber' => 'FR95305405045',
                'site' => 'site_154',
            ),
        );

        foreach ($objects as $key => $object) {
            $bank = new Bank();
            $bank->setBranchCode($object['branchCode']);
            $bank->setBankName($object['bankName']);
            $bank->setSortCode($object['sortCode']);
            $bank->setAccountNumber($object['accountNumber']);
            $bank->setRibKey($object['ribKey']);
            $bank->setIban($object['iban']);
            $bank->setTvaIntracomNumber($object['tvaIntracomNumber']);

            if ($object['site']) {
                $bank->setSite($this->getReference($object['site']));
            }

            $manager->persist($bank);
            $this->addReference($key, $bank);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}
