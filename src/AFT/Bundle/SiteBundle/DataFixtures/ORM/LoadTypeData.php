<?php

namespace AFT\Bundle\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AFT\Bundle\SiteBundle\Entity\Type;

class LoadTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'type_1' => array(
                'code' => 'entite',
                'label' => 'Entité',
                'shortLabel' => 'Ent',
            ),
            'type_2' => array(
                'code' => 'direction',
                'label' => 'Direction',
                'shortLabel' => 'Dir',
            ),
            'type_3' => array(
                'code' => 'ir',
                'label' => 'Inter-région',
                'shortLabel' => 'IR',
            ),
            'type_4' => array(
                'code' => 'region',
                'label' => 'Région',
                'shortLabel' => 'Reg',
            ),
            'type_5' => array(
                'code' => 'centre',
                'label' => 'Centre',
                'shortLabel' => 'Cent',
            ),
            'type_6' => array(
                'code' => 'antenne',
                'label' => 'Antenne',
                'shortLabel' => 'Ant',
            ),
        );

        foreach ($objects as $key => $object) {
            $type = new Type();
            $type->setCode($object['code']);
            $type->setLabel($object['label']);
            $type->setShortLabel($object['shortLabel']);

            $manager->persist($type);

            $this->addReference($key, $type);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
