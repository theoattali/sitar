<?php

namespace AFT\Bundle\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AFT\Bundle\SiteBundle\Entity\Role;

class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'role_1' => array(
                'code' => 'president',
                'label' => 'Président',
            ),
            'role_2' => array(
                'code' => 'dr',
                'label' => 'Directeur de Régional',
            ),
            'role_3' => array(
                'code' => 'dir',
                'label' => 'Directeur d\'Inter-région',
            ),
            'role_4' => array(
                'code' => 'dcf',
                'label' => 'Directeur de Centre de Formation',
            ),
        );

        foreach ($objects as $key => $object) {
            $role = new Role();
            $role->setCode($object['code']);
            $role->setLabel($object['label']);

            $manager->persist($role);
            $this->addReference($key, $role);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
