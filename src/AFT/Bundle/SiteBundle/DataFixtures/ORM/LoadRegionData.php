<?php

namespace AFT\Bundle\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AFT\Bundle\SiteBundle\Entity\Region;

class LoadRegionData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'region_1' => array(
                'code_insee' => '42',
                'label' => 'Alsace',
            ),
            'region_2' => array(
                'code_insee' => '72',
                'label' => 'Aquitaine',
            ),
            'region_3' => array(
                'code_insee' => '83',
                'label' => 'Auvergne',
            ),
            'region_4' => array(
                'code_insee' => '25',
                'label' => 'Basse-Normandie',
            ),
            'region_5' => array(
                'code_insee' => '26',
                'label' => 'Bourgogne',
            ),
            'region_6' => array(
                'code_insee' => '53',
                'label' => 'Bretagne',
            ),
            'region_7' => array(
                'code_insee' => '24',
                'label' => 'Centre',
            ),
            'region_8' => array(
                'code_insee' => '21',
                'label' => 'Champagne-Ardenne',
            ),
            'region_9' => array(
                'code_insee' => '94',
                'label' => 'Corse',
            ),
            'region_10' => array(
                'code_insee' => '43',
                'label' => 'Franche-Comté',
            ),
            'region_11' => array(
                'code_insee' => '01',
                'label' => 'Guadeloupe',
            ),
            'region_12' => array(
                'code_insee' => '03',
                'label' => 'Guyane',
            ),
            'region_13' => array(
                'code_insee' => '23',
                'label' => 'Haute-Normandie',
            ),
            'region_14' => array(
                'code_insee' => '11',
                'label' => 'Île-de-France',
            ),
            'region_15' => array(
                'code_insee' => '91',
                'label' => 'Languedoc-Roussillon',
            ),
            'region_16' => array(
                'code_insee' => '04',
                'label' => 'La Réunion',
            ),
            'region_17' => array(
                'code_insee' => '74',
                'label' => 'Limousin',
            ),
            'region_18' => array(
                'code_insee' => '41',
                'label' => 'Lorraine',
            ),
            'region_19' => array(
                'code_insee' => '02',
                'label' => 'Martinique',
            ),
            'region_20' => array(
                'code_insee' => '06',
                'label' => 'Mayotte',
            ),
            'region_21' => array(
                'code_insee' => '73',
                'label' => 'Midi-Pyrénées',
            ),
            'region_22' => array(
                'code_insee' => '31',
                'label' => 'Nord-Pas-de-Calais',
            ),
            'region_23' => array(
                'code_insee' => '52',
                'label' => 'Pays de la Loire',
            ),
            'region_24' => array(
                'code_insee' => '22',
                'label' => 'Picardie',
            ),
            'region_25' => array(
                'code_insee' => '54',
                'label' => 'Poitou-Charentes',
            ),
            'region_26' => array(
                'code_insee' => '93',
                'label' => 'Provence-Alpes-Côte d\'Azur',
            ),
            'region_27' => array(
                'code_insee' => '82',
                'label' => 'Rhône-Alpes',
            ),
        );

        foreach ($objects as $key => $object) {
            $region = new Region();
            $region->setCodeInsee($object['code_insee']);
            $region->setLabel($object['label']);

            $manager->persist($region);

            $this->addReference($key, $region);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
