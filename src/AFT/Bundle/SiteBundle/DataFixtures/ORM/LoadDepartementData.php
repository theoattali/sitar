<?php

namespace AFT\Bundle\SiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AFT\Bundle\SiteBundle\Entity\Departement;

class LoadDepartementData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'departement_1' => array(
                'code_insee' => '01',
                'label' => 'Ain',
                'region' => 'region_27',
            ),
            'departement_2' => array(
                'code_insee' => '02',
                'label' => 'Aisne',
                'region' => 'region_24',
            ),
            'departement_3' => array(
                'code_insee' => '03',
                'label' => 'Allier',
                'region' => 'region_3',
            ),
            'departement_4' => array(
                'code_insee' => '04',
                'label' => 'Alpes-de-Haute-Provence',
                'region' => 'region_26',
            ),
            'departement_5' => array(
                'code_insee' => '05',
                'label' => 'Hautes-Alpes',
                'region' => 'region_26',
            ),
            'departement_6' => array(
                'code_insee' => '06',
                'label' => 'Alpes-Maritimes',
                'region' => 'region_26',
            ),
            'departement_7' => array(
                'code_insee' => '07',
                'label' => 'Ardèche',
                'region' => 'region_27',
            ),
            'departement_8' => array(
                'code_insee' => '08',
                'label' => 'Ardennes',
                'region' => 'region_8',
            ),
            'departement_9' => array(
                'code_insee' => '09',
                'label' => 'Ariège',
                'region' => 'region_21',
            ),
            'departement_10' => array(
                'code_insee' => '10',
                'label' => 'Aube',
                'region' => 'region_8',
            ),
            'departement_11' => array(
                'code_insee' => '11',
                'label' => 'Aude',
                'region' => 'region_15',
            ),
            'departement_12' => array(
                'code_insee' => '12',
                'label' => 'Aveyron',
                'region' => 'region_21',
            ),
            'departement_13' => array(
                'code_insee' => '13',
                'label' => 'Bouches-du-Rhône',
                'region' => 'region_26',
            ),
            'departement_14' => array(
                'code_insee' => '14',
                'label' => 'Calvados',
                'region' => 'region_4',
            ),
            'departement_15' => array(
                'code_insee' => '15',
                'label' => 'Cantal',
                'region' => 'region_3',
            ),
            'departement_16' => array(
                'code_insee' => '16',
                'label' => 'Charente',
                'region' => 'region_25',
            ),
            'departement_17' => array(
                'code_insee' => '17',
                'label' => 'Charente-Maritime',
                'region' => 'region_25',
            ),
            'departement_18' => array(
                'code_insee' => '18',
                'label' => 'Cher',
                'region' => 'region_7',
            ),
            'departement_19' => array(
                'code_insee' => '19',
                'label' => 'Corrèze',
                'region' => 'region_17',
            ),
            'departement_20' => array(
                'code_insee' => '21',
                'label' => 'Côte-d\'Or',
                'region' => 'region_5',
            ),
            'departement_21' => array(
                'code_insee' => '22',
                'label' => 'Côtes-d\'Armor',
                'region' => 'region_6',
            ),
            'departement_22' => array(
                'code_insee' => '23',
                'label' => 'Creuse',
                'region' => 'region_17',
            ),
            'departement_23' => array(
                'code_insee' => '24',
                'label' => 'Dordogne',
                'region' => 'region_2',
            ),
            'departement_24' => array(
                'code_insee' => '25',
                'label' => 'Doubs',
                'region' => 'region_10',
            ),
            'departement_25' => array(
                'code_insee' => '26',
                'label' => 'Drôme',
                'region' => 'region_27',
            ),
            'departement_26' => array(
                'code_insee' => '27',
                'label' => 'Eure',
                'region' => 'region_13',
            ),
            'departement_27' => array(
                'code_insee' => '28',
                'label' => 'Eure-et-Loir',
                'region' => 'region_7',
            ),
            'departement_28' => array(
                'code_insee' => '29',
                'label' => 'Finistère',
                'region' => 'region_6',
            ),
            'departement_29' => array(
                'code_insee' => '2A',
                'label' => 'Corse-du-Sud',
                'region' => 'region_9',
            ),
            'departement_30' => array(
                'code_insee' => '2B',
                'label' => 'Haute-Corse',
                'region' => 'region_9',
            ),
            'departement_31' => array(
                'code_insee' => '30',
                'label' => 'Gard',
                'region' => 'region_15',
            ),
            'departement_32' => array(
                'code_insee' => '31',
                'label' => 'Haute-Garonne',
                'region' => 'region_21',
            ),
            'departement_33' => array(
                'code_insee' => '32',
                'label' => 'Gers',
                'region' => 'region_21',
            ),
            'departement_34' => array(
                'code_insee' => '33',
                'label' => 'Gironde',
                'region' => 'region_2',
            ),
            'departement_35' => array(
                'code_insee' => '34',
                'label' => 'Hérault',
                'region' => 'region_15',
            ),
            'departement_36' => array(
                'code_insee' => '35',
                'label' => 'Ille-et-Vilaine',
                'region' => 'region_6',
            ),
            'departement_37' => array(
                'code_insee' => '36',
                'label' => 'Indre',
                'region' => 'region_7',
            ),
            'departement_38' => array(
                'code_insee' => '37',
                'label' => 'Indre-et-Loire',
                'region' => 'region_7',
            ),
            'departement_39' => array(
                'code_insee' => '38',
                'label' => 'Isère',
                'region' => 'region_27',
            ),
            'departement_40' => array(
                'code_insee' => '39',
                'label' => 'Jura',
                'region' => 'region_10',
            ),
            'departement_41' => array(
                'code_insee' => '40',
                'label' => 'Landes',
                'region' => 'region_2',
            ),
            'departement_42' => array(
                'code_insee' => '41',
                'label' => 'Loir-et-Cher',
                'region' => 'region_7',
            ),
            'departement_43' => array(
                'code_insee' => '42',
                'label' => 'Loire',
                'region' => 'region_27',
            ),
            'departement_44' => array(
                'code_insee' => '43',
                'label' => 'Haute-Loire',
                'region' => 'region_3',
            ),
            'departement_45' => array(
                'code_insee' => '44',
                'label' => 'Loire-Atlantique',
                'region' => 'region_23',
            ),
            'departement_46' => array(
                'code_insee' => '45',
                'label' => 'Loiret',
                'region' => 'region_7',
            ),
            'departement_47' => array(
                'code_insee' => '46',
                'label' => 'Lot',
                'region' => 'region_21',
            ),
            'departement_48' => array(
                'code_insee' => '47',
                'label' => 'Lot-et-Garonne',
                'region' => 'region_2',
            ),
            'departement_49' => array(
                'code_insee' => '48',
                'label' => 'Lozère',
                'region' => 'region_15',
            ),
            'departement_50' => array(
                'code_insee' => '49',
                'label' => 'Maine-et-Loire',
                'region' => 'region_23',
            ),
            'departement_51' => array(
                'code_insee' => '50',
                'label' => 'Manche',
                'region' => 'region_4',
            ),
            'departement_52' => array(
                'code_insee' => '51',
                'label' => 'Marne',
                'region' => 'region_8',
            ),
            'departement_53' => array(
                'code_insee' => '52',
                'label' => 'Haute-Marne',
                'region' => 'region_8',
            ),
            'departement_54' => array(
                'code_insee' => '53',
                'label' => 'Mayenne',
                'region' => 'region_23',
            ),
            'departement_55' => array(
                'code_insee' => '54',
                'label' => 'Meurthe-et-Moselle',
                'region' => 'region_18',
            ),
            'departement_56' => array(
                'code_insee' => '55',
                'label' => 'Meuse',
                'region' => 'region_18',
            ),
            'departement_57' => array(
                'code_insee' => '56',
                'label' => 'Morbihan',
                'region' => 'region_6',
            ),
            'departement_58' => array(
                'code_insee' => '57',
                'label' => 'Moselle',
                'region' => 'region_18',
            ),
            'departement_59' => array(
                'code_insee' => '58',
                'label' => 'Nièvre',
                'region' => 'region_5',
            ),
            'departement_60' => array(
                'code_insee' => '59',
                'label' => 'Nord',
                'region' => 'region_22',
            ),
            'departement_61' => array(
                'code_insee' => '60',
                'label' => 'Oise',
                'region' => 'region_24',
            ),
            'departement_62' => array(
                'code_insee' => '61',
                'label' => 'Orne',
                'region' => 'region_4',
            ),
            'departement_63' => array(
                'code_insee' => '62',
                'label' => 'Pas-de-Calais',
                'region' => 'region_22',
            ),
            'departement_64' => array(
                'code_insee' => '63',
                'label' => 'Puy-de-Dôme',
                'region' => 'region_3',
            ),
            'departement_65' => array(
                'code_insee' => '64',
                'label' => 'Pyrénées-Atlantiques',
                'region' => 'region_2',
            ),
            'departement_66' => array(
                'code_insee' => '65',
                'label' => 'Hautes-Pyrénées',
                'region' => 'region_21',
            ),
            'departement_67' => array(
                'code_insee' => '66',
                'label' => 'Pyrénées-Orientales',
                'region' => 'region_15',
            ),
            'departement_68' => array(
                'code_insee' => '67',
                'label' => 'Bas-Rhin',
                'region' => 'region_1',
            ),
            'departement_69' => array(
                'code_insee' => '68',
                'label' => 'Haut-Rhin',
                'region' => 'region_1',
            ),
            'departement_70' => array(
                'code_insee' => '69',
                'label' => 'Rhône',
                'region' => 'region_27',
            ),
            'departement_71' => array(
                'code_insee' => '70',
                'label' => 'Haute-Saône',
                'region' => 'region_10',
            ),
            'departement_72' => array(
                'code_insee' => '71',
                'label' => 'Saône-et-Loire',
                'region' => 'region_5',
            ),
            'departement_73' => array(
                'code_insee' => '72',
                'label' => 'Sarthe',
                'region' => 'region_23',
            ),
            'departement_74' => array(
                'code_insee' => '73',
                'label' => 'Savoie',
                'region' => 'region_27',
            ),
            'departement_75' => array(
                'code_insee' => '74',
                'label' => 'Haute-Savoie',
                'region' => 'region_27',
            ),
            'departement_76' => array(
                'code_insee' => '75',
                'label' => 'Paris',
                'region' => 'region_14',
            ),
            'departement_77' => array(
                'code_insee' => '76',
                'label' => 'Seine-Maritime',
                'region' => 'region_13',
            ),
            'departement_78' => array(
                'code_insee' => '77',
                'label' => 'Seine-et-Marne',
                'region' => 'region_14',
            ),
            'departement_79' => array(
                'code_insee' => '78',
                'label' => 'Yvelines',
                'region' => 'region_14',
            ),
            'departement_80' => array(
                'code_insee' => '79',
                'label' => 'Deux-Sèvres',
                'region' => 'region_25',
            ),
            'departement_81' => array(
                'code_insee' => '80',
                'label' => 'Somme',
                'region' => 'region_24',
            ),
            'departement_82' => array(
                'code_insee' => '81',
                'label' => 'Tarn',
                'region' => 'region_21',
            ),
            'departement_83' => array(
                'code_insee' => '82',
                'label' => 'Tarn-et-Garonne',
                'region' => 'region_21',
            ),
            'departement_84' => array(
                'code_insee' => '83',
                'label' => 'Var',
                'region' => 'region_26',
            ),
            'departement_85' => array(
                'code_insee' => '84',
                'label' => 'Vaucluse',
                'region' => 'region_26',
            ),
            'departement_86' => array(
                'code_insee' => '85',
                'label' => 'Vendée',
                'region' => 'region_23',
            ),
            'departement_87' => array(
                'code_insee' => '86',
                'label' => 'Vienne',
                'region' => 'region_25',
            ),
            'departement_88' => array(
                'code_insee' => '87',
                'label' => 'Haute-Vienne',
                'region' => 'region_17',
            ),
            'departement_89' => array(
                'code_insee' => '88',
                'label' => 'Vosges',
                'region' => 'region_18',
            ),
            'departement_90' => array(
                'code_insee' => '89',
                'label' => 'Yonne',
                'region' => 'region_5',
            ),
            'departement_91' => array(
                'code_insee' => '90',
                'label' => 'Territoire-de-Belfort',
                'region' => 'region_10',
            ),
            'departement_92' => array(
                'code_insee' => '91',
                'label' => 'Essonne',
                'region' => 'region_14',
            ),
            'departement_93' => array(
                'code_insee' => '92',
                'label' => 'Hauts-de-Seine',
                'region' => 'region_14',
            ),
            'departement_94' => array(
                'code_insee' => '93',
                'label' => 'Seine-Saint-Denis',
                'region' => 'region_14',
            ),
            'departement_95' => array(
                'code_insee' => '94',
                'label' => 'Val-de-Marne',
                'region' => 'region_14',
            ),
            'departement_96' => array(
                'code_insee' => '95',
                'label' => 'Val-d\'Oise',
                'region' => 'region_14',
            ),
            'departement_97' => array(
                'code_insee' => '971',
                'label' => 'Guadeloupe',
                'region' => 'region_11',
            ),
            'departement_98' => array(
                'code_insee' => '972',
                'label' => 'Martinique',
                'region' => 'region_19',
            ),
            'departement_99' => array(
                'code_insee' => '973',
                'label' => 'Guyane',
                'region' => 'region_12',
            ),
            'departement_100' => array(
                'code_insee' => '974',
                'label' => 'La Réunion',
                'region' => 'region_16',
            ),
            'departement_101' => array(
                'code_insee' => '976',
                'label' => 'Mayotte',
                'region' => 'region_20',
            ),

        );

        foreach ($objects as $key => $object) {
            $departement = new Departement();
            $departement->setCodeInsee($object['code_insee']);
            $departement->setLabel($object['label']);
            $departement->setRegion($this->getReference($object['region']));

            $manager->persist($departement);

            $this->addReference($key, $departement);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
