<?php

namespace AFT\Bundle\SiteBundle\Repository;

use AFT\Bundle\SiteBundle\Entity\Site;
use AFT\Bundle\SiteBundle\Domain\RestSearchSiteDomain;
use Doctrine\ORM\EntityRepository;

class SiteRepository extends EntityRepository
{
    public function search(Site $site)
    {
        $qb = $this->prepareSearch($site);

        if (null !== $site->getType()) {
            $qb
                ->andWhere('s.type = :type')
                ->setParameter('type', $site->getType());
        }

        return $qb->orderBy('s.corporateName', 'ASC')->getQuery()->getResult();
    }

    public function restSearch(RestSearchSiteDomain $restSearchSite)
    {
        $qb = $this->prepareSearch($restSearchSite);

        if (null !== $restSearchSite->getType()) {
            $qb
                ->innerJoin('s.type', 't')
                ->where('t.code = :type')
                ->setParameter('type', $restSearchSite->getType());
        }

        if (null !== $restSearchSite->getLimit()) {
            $qb
                ->setMaxResults($restSearchSite->getLimit());
        }
        return $qb->orderBy('s.corporateName', 'ASC')->getQuery()->getResult();
    }

    private function prepareSearch($site)
    {
        $qb = $this->createQueryBuilder('s');

        if (null !== $site->getCode()) {
            $qb
                ->andWhere('s.code LIKE :code')
                ->setParameter('code', $site->getCode().'%');
        }

        if (null !== $site->getCorporateName()) {
            $qb
                ->andWhere('s.corporateName LIKE :corporateName')
                ->setParameter('corporateName', $site->getCorporateName().'%');
        }

        if (null !== $site->getName()) {
            $qb
                ->andWhere('s.name LIKE :name')
                ->setParameter('name', $site->getName().'%');
        }

        return $qb;
    }
}
