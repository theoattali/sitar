<?php

namespace AFT\Bundle\SiteBundle\Controller;

use AFT\Bundle\SiteBundle\Entity\Site;
use AFT\Bundle\SiteBundle\Form\Type\RestSearchType;
use AFT\Bundle\RestBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RestSearchController extends Controller
{
    public function getData(Site $site)
    {
        $data = array(
            'id' => $site->getId(),
            'code' => $site->getCode(),
            'corporateName' => $site->getCorporateName(),
            'name' => $site->getName(),
            'type' => $site->getType()->getLabel(),
        );

        foreach ($site->getAddresses() as $key => $address) {
            $data['addresses'][] = array(
                'streetNumber' => $address->getStreetNumber(),
                'streetLine1' => $address->getStreetLine1(),
                'streetLine2' => $address->getStreetLine2(),
                'streetLine3' => $address->getStreetLine3(),
                'streetLine4' => $address->getStreetLine4(),
                'zipCode' => $address->getZipCode(),
                'cityName' => $address->getCityName(),
            );
        }

        return $data;
    }

    public function getByIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository('AFTSiteBundle:Site')->findOneById($id);

        if (!$site) {
            return $this->notFound('Le site '.$id.' n\'a pas été trouvé');
        }

        $data = array('site' => $this->getData($site));

        $parent = $site->getParent();
        if (null !== $parent) {
            $data['site']['parent'] = array(
                'id' => $parent->getId(),
                'code' => $parent->getCode(),
                'name' => $parent->getName(),
                'type' => $parent->getType()->getLabel(),
            );
        }

        $property = $site->getProperty();
        if (null !== $property) {
            $data['site']['property'] = array(
                'adonixCode' => $property->getAdonixCode(),
                'territoryCode' => $property->getTerritoryCode(),
                'selligentCode' => $property->getSelligentCode(),
                'aftManagementCode' => $property->getAftManagementCode(),
                'iftimManagementCode' => $property->getIftimManagementCode(),
                'aftralManagementCode' => $property->getAftralManagementCode(),
                'transportCode' => $property->getTransportCode(),
                'logisticCode' => $property->getLogisticCode(),
                'backgroundColor' => $property->getBackgroundColor(),
                'textColor' => $property->getTextColor(),
                'cqcNumber' => $property->getCqcNumber(),
                'cadrNumber' => $property->getCadrNumber(),
                'adCode' => $property->getAdCode(),
                'serverIp' => $property->getServerIp(),
                'serverName' => $property->getServerName(),
                'virtualNdv' => $property->getVirtualNdv(),
            );
        }

        return $this->json($data);
    }

    public function searchSiteAction(Request $request)
    {
        $form = $this->createForm(RestSearchType::class, null, array(
            'validation_groups' => array('restSearch'),
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $sites = $this->getDoctrine()
                ->getRepository('AFTSiteBundle:Site')
                ->restSearch($form->getData());
        }
        else {
            return $this->badRequest($this->prepareErrorMessage($form));
        }

        $searchSite = array();
        foreach ($sites as $site) {
            $searchSite[] = $this->getData($site);
        }

        return $this->json($searchSite);
    }
}
