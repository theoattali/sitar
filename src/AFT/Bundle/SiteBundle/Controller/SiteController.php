<?php

namespace AFT\Bundle\SiteBundle\Controller;

use AFT\Bundle\SiteBundle\Entity;
use AFT\Bundle\SiteBundle\Form\Type;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends Controller
{
    public function searchAction(Request $request)
    {
        $form = $this->createForm(Type\SiteSearchType::class, new Entity\Site(), array(
            'validation_groups' => array('search'),
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $sites = $this->getDoctrine()
                    ->getRepository('AFTSiteBundle:Site')
                    ->search($form->getData());

                return $this->render('AFTSiteBundle:Site:result.html.twig', array(
                    'sites' => $sites,
                    'form' => $form->createView(),
                ));

            }
        }

        return $this->render('AFTSiteBundle:Site:search.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function newAction(Request $request)
    {
        $site = new Entity\Site();

        $form = $this->createForm(Type\SiteType::class, $site, array(
            'validation_groups' => array('create'),
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($site);
            $em->flush();

            return $this->redirectToRoute('site_show', array('id' => $site->getId()));
        }

        return $this->render('AFTSiteBundle:Site:edit.html.twig', array(
            'form' => $form->createView(),
            'type' => 'new',
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository('AFTSiteBundle:Site')->findOneById($id);

        if (!$site) {
            throw $this->createNotFoundException('Le site '.$id.' n\'a pas été trouvé');
        }

        return $this->render('AFTSiteBundle:Site:show.html.twig', array(
            'site' => $site,
        ));
    }

    public function logAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository('AFTSiteBundle:Site')->findOneById($id);

        if (!$site) {
            throw $this->createNotFoundException('Le site '.$id.' n\'a pas été trouvé');
        }

        $logs = $em->getRepository('AFTSiteBundle:Log')->getLogEntries($site);

        foreach ($logs as $log) {
            $data = $log->getData();
            if (isset($data['type'])) {
                $data['type'] = $em->getRepository('AFTSiteBundle:Type')->findOneBy(['id' => $data['type']]);
                $log->setData($data);
            }
            if (isset($data['supervisor'])) {
                $data['supervisor'] = $em->getRepository('AFTSiteBundle:Supervisor')->findOneBy(['id' => $data['supervisor']]);
                $log->setData($data);
            }
            if (isset($data['parent'])) {
                $data['parent'] = $em->getRepository('AFTSiteBundle:Site')->findOneBy(['id' => $data['parent']]);
                $log->setData($data);
            }
        }

        return $this->render('AFTSiteBundle:Site:log.html.twig', array(
            'logs' => $logs,
            'site' => $site,
        ));
    }

    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository('AFTSiteBundle:Site')->findOneById($id);

        if (!$site) {
            throw $this->createNotFoundException('Le site '.$id.' n\'a pas été trouvé');
        }

        $form = $this->createForm(Type\SiteType::class, $site, array(
            'validation_groups' => array('edit'),
            'extended_form' => true,
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $em->persist($site);

                $em->flush();

                return $this->redirectToRoute('site_show', array('id' => $site->getId()));
            }
        }

        return $this->render('AFTSiteBundle:Site:edit.html.twig', array(
            'site' => $site,
            'form' => $form->createView(),
            'type' => 'edit',
        ));
    }

    public function editComponentAction(Request $request, $id, $component)
    {
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository('AFTSiteBundle:Site')->findOneById($id);

        if (!$site) {
            throw $this->createNotFoundException('Le site '.$id.' n\'a pas été trouvé');
        }

        if ('address' === $component) {
            $adresses = new ArrayCollection();

            foreach ($site->getAddresses() as $address) {
                $adresses->add($address);
            }
        }

        $form = $this->createForm(Type\SiteComponentType::class, $site, array(
            'validation_groups' => array('edit'),
            'component' => $component,
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {

                if ('address' === $component) {
                    foreach ($adresses as $address) {
                        if (false === $site->getAddresses()->contains($address)) {
                            $em->remove($address);
                        }
                    }
                }

                $em->persist($site);
                $em->flush();

                return $this->redirectToRoute('site_show', array('id' => $site->getId()));
            }
        }

        return $this->render('AFTSiteBundle:Site:editComp.html.twig', array(
            'site' => $site,
            'form' => $form->createView(),
            'component' => $component,
        ));
    }

    public function disableAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository('AFTSiteBundle:Site')->findOneById($id);

        if (!$site) {
            throw $this->createNotFoundException('Le site '.$id.' n\'a pas été trouvé');
        }

        $form = $this->createForm(Type\SiteDisableType::class, $site, array(
            'validation_groups' => array('disable'),
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $today = new \DateTime();
            $today->setTime(0, 0, 0);
            if ($site->getDisabledAt() <= $today) {
                $childs = $em->getRepository('AFTSiteBundle:Site')->findByParent($id);

                foreach ($childs as $child) {
                    $child->setParent($site->getSuccessor());
                }
            }

            $em->flush();

            return $this->redirectToRoute('site_search');
        }

        return $this->render('AFTSiteBundle:Site:disable.html.twig', array(
            'site' => $site,
            'form' => $form->createView(),
        ));
    }
}
