<?php

namespace AFT\Bundle\SiteBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DisableCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('aft:site:disable')
            ->setDescription('Disable sites')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $disabledAt = new \DateTime();
        $disabledAt->setTime(0, 0, 0);
        $output->writeln(sprintf('Search sites disabled the <info>%s</info>', $disabledAt->format('Y-m-d')));

        $sites = $em->getRepository('AFTSiteBundle:Site')->createQueryBuilder('s')
            ->innerJoin('s.parent', 's_parent')
            ->andWhere('s_parent.disabledAt = :disabled_at')
            ->setParameter('disabled_at', $disabledAt)
            ->getQuery()
            ->execute()
        ;

        $nbSites = count($sites);
        $output->writeln(sprintf('<info>%u children found</info>', $nbSites));
        if (0 === $nbSites) {
            return;
        }

        $this->getContainer()->get('stof_doctrine_extensions.listener.blameable')->setUserValue('console');
        foreach ($sites as $site) {
            $site->setParent($site->getParent()->getSuccessor());
        }

        $output->writeln('<info>Flush new parents</info>');
        $em->flush();
    }
}
