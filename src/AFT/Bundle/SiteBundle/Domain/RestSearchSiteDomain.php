<?php

namespace AFT\Bundle\SiteBundle\Domain;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class RestSearchSiteDomain
{
    /**
     * @Assert\Length(
     *     min = "6",
     *     max = "6",
     *     exactMessage = "Le code doit être constitué de {{ limit }} caractères.",
     *     groups = { "restSearch" }
     * )
     */
    private $code;

    /**
     * @Assert\Length(
     *     min = "3",
     *     max = "100",
     *     minMessage = "La raison sociale doit être constituée d'au moins {{ limit }} caractères.",
     *     maxMessage = "La raison sociale doit être constituée de {{ limit }} caractères maximum.",
     *     groups = { "restSearch" }
     * )
     */
    private $corporateName;

    /**
     * @Assert\Length(
     *     min = "3",
     *     max = "100",
     *     minMessage = "Le nom doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le nom doit être constitué de {{ limit }} caractères maximum.",
     *     groups = { "restSearch" }
     * )
     */
    private $name;

    /**
     * @Assert\Length(
     *     min = "2",
     *     minMessage = "Le type doit être constitué d'au moins {{ limit }} caractères.",
     *     groups = { "restSearch" }
     * )
     */
    private $type;

    /**
     * @Assert\Regex(
     *     pattern = "/(?<=\s|^)\d+(?=\s|$)/",
     *     message = "La limite doit être un nombre.",
     *     groups = { "restSearch" }
     * )
     */
    private $limit;


    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;
    }

    public function getCorporateName()
    {
        return $this->corporateName;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }
}
