<?php

namespace AFT\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AFT\Bundle\SiteBundle\Repository\SiteRepository")
 */
class Departement
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="code_insee", type="string", length=10)
     */
    private $codeInsee;

    /**
     * @ORM\Column(name="label", type="string", length=100)
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity="Region", cascade={"persist", "remove"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $region;


    public function getId()
    {
        return $this->id;
    }

    public function setCodeInsee($codeInsee)
    {
        $this->codeInsee = $codeInsee;
    }

    public function getCodeInsee()
    {
        return $this->codeInsee;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setRegion(Region $region)
    {
        $this->region = $region;
    }

    public function getRegion()
    {
        return $this->region;
    }
}
