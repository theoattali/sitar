<?php

namespace AFT\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="AFT\Bundle\SiteBundle\Repository\SiteRepository")
 * @Gedmo\Loggable(logEntryClass="Log")
 */
class Site
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="code", type="string", length=10)
     * @Assert\NotBlank(
     *     message = "Le code ne peut être ignoré.",
     *     groups={"create", "edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "6",
     *     minMessage = "Le code doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"search"}
     * )
     * @Assert\Length(
     *     min = "6",
     *     max = "6",
     *     exactMessage = "Le code doit être constitué de {{ limit }} caractères.",
     *     groups={"create", "edit"}
     * )
     */
    private $code;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="corporate_name", type="string", length=100)
     * @Assert\NotBlank(
     *     message = "La raison sociale ne peut être ignorée.",
     *     groups={"create", "edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "100",
     *     minMessage = "La raison sociale doit être constituée d'au moins {{ limit }} caractères.",
     *     maxMessage = "La raison sociale doit être constituée de {{ limit }} caractères maximum.",
     *     groups = {"create", "edit", "search"}
     * )
     */
    private $corporateName;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank(
     *     message = "Le nom ne peut être ignoré.",
     *     groups={"create", "edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "100",
     *     minMessage = "Le nom doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le nom doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"create", "edit", "search"}
     * )
     */
    private $name;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="locality", type="string", length=100, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "100",
     *     minMessage = "La localité doit être constituée d'au moins {{ limit }} caractères.",
     *     maxMessage = "La localité doit être constituée de {{ limit }} caractères maximum.",
     *     groups = {"create", "edit"}
     * )
     */
    private $locality;

    /**
     * @ORM\Column(name="domain", type="string", length=50, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "50",
     *     minMessage = "Le domaine doit être constituée d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le domaine doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"create", "edit"}
     * )
     */
    private $domain;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="subject_tva", type="boolean")
     */
    private $subjectTva;

    /**
     * @ORM\Column(name="website_url", type="string", length=255)
     * @Assert\NotBlank(
     *     message = "Le site web ne peut être ignoré.",
     *     groups={"create", "edit"}
     * )
     * @Assert\Url(
     *     message = "Le site web est invalide",
     *     groups={"create", "edit"}
     * )
     */
    private $websiteUrl;

    /**
     * @ORM\Column(name="mail", type="string", length=255)
     * @Assert\NotBlank(
     *     message = "L'adresse mail ne peut être ignorée.",
     *     groups={"create", "edit"}
     * )
     * @Assert\Email(
     *     message = "L'adresse mail est invalide",
     *     groups={"create", "edit"}
     * )
     */
    private $mail;

    /**
     * @ORM\Column(name="opening_time", type="string", length=50, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "50",
     *     minMessage = "Les horaires d'ouverture doivent être constituées d'au moins {{ limit }} caractères.",
     *     maxMessage = "Les horaires d'ouverture doivent être constituées de {{ limit }} caractères maximum.",
     *     groups = {"create", "edit"}
     * )
     */
    private $openingTime;

    /**
     * @ORM\Column(name="num_siren", type="string", length=10, nullable=true)
     * @Assert\Length(
     *     min = "9",
     *     max = "9",
     *     exactMessage = "Le numéro SIREN doit être constitué de {{ limit }} caractères.",
     *     groups = {"create", "edit"}
     * )
     */
    private $numSiren;

    /**
     * @ORM\Column(name="num_ic", type="string", length=10, nullable=true)
     * @Assert\Length(
     *     min = "5",
     *     max = "5",
     *     exactMessage = "Le numéro IC doit être constitué de {{ limit }} caractères.",
     *     groups = {"create", "edit"}
     * )
     */
    private $numIc;

    /**
     * @ORM\Column(name="code_ape_naf", type="string", length=20, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "20",
     *     minMessage = "Le code APE (NAF) doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code APE (NAF) doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"create", "edit"}
     * )
     */
    private $codeApeNaf;

    /**
     * @ORM\Column(name="activity_number", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le numéro d'activité doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le numéro d'activité doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"create", "edit"}
     * )
     */
    private $activityNumber;

    /**
     * @ORM\Column(name="begining_activity_at", type="date")
     * @Assert\NotBlank(
     *     message = "Le numéro d'activité ne peut être ignoré.",
     *     groups={"create", "edit"}
     * )
     */
    private $beginingActivityAt;

    /**
    * @Gedmo\Blameable(on="create")
     * @ORM\Column(name="created_by", type="string", length=100)
     */
    private $createdBy;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
    * @Gedmo\Blameable(on="update")
     * @ORM\Column(name="updated_by", type="string", length=100)
     */
    private $updatedBy;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="Type")
     * @Assert\NotBlank(
     *     message = "Le type ne peut être ignoré.",
     *     groups={"create", "edit"}
     * )
     */
    private $type;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="Supervisor")
     * @ORM\JoinColumn(nullable=true)
     */
    private $supervisor;

    /**
     * @ORM\OneToMany(targetEntity="Address", mappedBy="site", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    protected $addresses;

    /**
     * @ORM\OneToOne(targetEntity="Property", mappedBy="site", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $property;

    /**
     * @ORM\OneToOne(targetEntity="Bank", mappedBy="site", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $bank;

    /**
     * @ORM\OneToOne(targetEntity="Billing", mappedBy="site", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $billing;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="Site", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;

    /**
     * @ORM\OneToOne(targetEntity="Site")
     * @ORM\JoinColumn(nullable=true)
     */
    private $successor;


    public function __construct()
    {
        $this->addresses = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;
    }

    public function getCorporateName()
    {
        return $this->corporateName;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setLocality($locality = null)
    {
        $this->locality = $locality;
    }

    public function getLocality()
    {
        return $this->locality;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setSubjectTva($subjectTva = false)
    {
        $this->subjectTva = $subjectTva;
    }

    public function getSubjectTva()
    {
        return $this->subjectTva;
    }

    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;
    }

    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    public function getMail()
    {
        return $this->mail;
    }

    public function setOpeningTime($openingTime = null)
    {
        $this->openingTime = $openingTime;
    }

    public function getOpeningTime()
    {
        return $this->openingTime;
    }

    public function setNumSiren($numSiren = null)
    {
        $this->numSiren = $numSiren;
    }

    public function getNumSiren()
    {
        return $this->numSiren;
    }

    public function setNumIc($numIc = null)
    {
        $this->numIc = $numIc;
    }

    public function getNumIc()
    {
        return $this->numIc;
    }

    public function setCodeApeNaf($codeApeNaf = null)
    {
        $this->codeApeNaf = $codeApeNaf;
    }

    public function getCodeApeNaf()
    {
        return $this->codeApeNaf;
    }

    public function setActivityNumber($activityNumber = null)
    {
        $this->activityNumber = $activityNumber;
    }

    public function getActivityNumber()
    {
        return $this->activityNumber;
    }

    public function setBeginingActivityAt($beginingActivityAt)
    {
        $this->beginingActivityAt = $beginingActivityAt;
    }

    public function getBeginingActivityAt()
    {
        return $this->beginingActivityAt;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;
    }

    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    public function isDisabled()
    {
        $today = new \DateTime();
        $today->setTime(0, 0, 0);

        $disabledAt = $this->disabledAt;

        if (null != $disabledAt && $today >= $disabledAt) {
            return true;
        }

        return false;
    }

    public function setType(Type $type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setParent(Site $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setSupervisor(Supervisor $supervisor = null)
    {
        $this->supervisor = $supervisor;
    }

    public function getSupervisor()
    {
        return $this->supervisor;
    }

    public function addAddress(Address $address)
    {
        $this->addresses->add($address);
        $address->setSite($this);
    }

    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);
    }

    public function getAddresses()
    {
        return $this->addresses;
    }

    public function setProperty(Property $property = null)
    {
        $this->property = $property;
        $property->setSite($this);
    }

    public function getProperty()
    {

        return $this->property;
    }

    public function setBank(Bank $bank = null)
    {
        $this->bank = $bank;
        $bank->setSite($this);
    }

    public function getBank()
    {
        return $this->bank;
    }

    public function setBilling(Billing $billing = null)
    {
        $this->billing = $billing;
        $billing->setSite($this);
    }

    public function getBilling()
    {
        return $this->billing;
    }

    public function setSuccessor(Site $successor = null)
    {
        $this->successor = $successor;
    }

    public function getSuccessor()
    {
        return $this->successor;
    }

    /**
     * @Assert\Callback(groups = {"search"})
     */
    public function isSearchValid(ExecutionContextInterface $context)
    {
        if (empty($this->code) && empty($this->corporateName) &&
            empty($this->name) && empty($this->type) ) {
            $context->addViolation('Aucun critère de recherche n\'a été renseigné.');
        }
    }
}
