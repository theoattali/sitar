<?php

namespace AFT\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AFT\Bundle\SiteBundle\Repository\SiteRepository")
 */
class Address
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="street_number", type="string", length=10, nullable=true)
     */
    private $streetNumber;

    /**
     * @ORM\Column(name="street_line_1", type="string", length=255)
     * @Assert\NotBlank(
     *     message = "Le nom de rue ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "255",
     *     minMessage = "Le nom de rue doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le nom de rue doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $streetLine1;

    /**
     * @ORM\Column(name="street_line_2", type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "255",
     *     minMessage = "Ce complément d'adresse doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Ce complément d'adresse doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $streetLine2;

    /**
     * @ORM\Column(name="street_line_3", type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "255",
     *     minMessage = "Ce complément d'adresse doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Ce complément d'adresse doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $streetLine3;

    /**
     * @ORM\Column(name="street_line_4", type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "255",
     *     minMessage = "Ce complément d'adresse doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Ce complément d'adresse doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $streetLine4;

    /**
     * @ORM\Column(name="zip_code", type="string", length=10)
     * @Assert\NotBlank(
     *     message = "Le code postal ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "5",
     *     max = "5",
     *     exactMessage = "Le code postal doit être constitué de {{ limit }} caractères.",
     *     groups = {"edit"}
     * )
     */
    private $zipCode;

    /**
     * @ORM\Column(name="city_code", type="string", length=10, nullable=true)
     */
    private $cityCode;

    /**
     * @ORM\Column(name="city_name", type="string", length=100)
     * @Assert\NotBlank(
     *     message = "Le nom de commune ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "100",
     *     minMessage = "Le nom de commune doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le nom de commune doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $cityName;

    /**
     * @ORM\Column(name="country", type="string", length=100)
     * @Assert\NotBlank(
     *     message = "Le pays ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Country(
     *     message = "Le pays n'est pas conforme.",
     *     groups = {"edit"}
     * )
     */
    private $country;

    /**
     * @ORM\Column(name="phone_code", type="string", length=10)
     * @Assert\NotBlank(
     *     message = "L'indice téléphonique ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "2",
     *     max = "10",
     *     minMessage = "L'indice téléphonique doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "L'indice téléphonique doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $phoneCode;

    /**
     * @ORM\Column(name="phone_number", type="string", length=20)
     * @Assert\NotBlank(
     *     message = "Le numéro de téléphone ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "13",
     *     max = "13",
     *     exactMessage = "Le numéro de téléphone doit être constitué de {{ limit }} caractères.",
     *     groups = {"edit"}
     * )
     */
    private $phoneNumber;

    /**
     * @ORM\Column(name="fax_number", type="string", length=20, nullable=true)
     * @Assert\Length(
     *     min = "13",
     *     max = "13",
     *     exactMessage = "Le numéro de fax doit être constitué de {{ limit }} caractères.",
     *     groups = {"edit"}
     * )
     */
    private $faxNumber;

    /**
     * @ORM\ManyToOne(targetEntity="Departement")
     * @ORM\JoinColumn(nullable=true)
     */
    private $departement;

    /**
     * @ORM\ManyToOne(targetEntity="Site", inversedBy="addresses")
     */
    private $site;


    public function getId()
    {
        return $this->id;
    }

    public function setStreetNumber($streetNumber = null)
    {
        $this->streetNumber = $streetNumber;
    }

    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    public function setStreetLine1($streetLine1)
    {
        $this->streetLine1 = $streetLine1;
    }

    public function getStreetLine1()
    {
        return $this->streetLine1;
    }

    public function setStreetLine2($streetLine2 = null)
    {
        $this->streetLine2 = $streetLine2;
    }

    public function getStreetLine2()
    {
        return $this->streetLine2;
    }

    public function setStreetLine3($streetLine3 = null)
    {
        $this->streetLine3 = $streetLine3;
    }

    public function getStreetLine3()
    {
        return $this->streetLine3;
    }

    public function setStreetLine4($streetLine4 = null)
    {
        $this->streetLine4 = $streetLine4;
    }

    public function getStreetLine4()
    {
        return $this->streetLine4;
    }

    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }

    public function setCityCode($cityCode = null)
    {
        $this->cityCode = $cityCode;
    }

    public function getCityCode()
    {
        return $this->cityCode;
    }

    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
    }

    public function getCityName()
    {
        return $this->cityName;
    }
    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getCountryFull()
    {
        return Intl::getRegionBundle()->getCountryName($this->country);
    }

    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;
    }

    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function setFaxNumber($faxNumber = null)
    {
        $this->faxNumber = $faxNumber;
    }

    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    public function setDepartement(Departement $departement = null)
    {
        $this->departement = $departement;
    }

    public function getDepartement()
    {
        return $this->departement;
    }

    public function setSite(Site $site)
    {
        $this->site = $site;
    }

    public function getSite()
    {
        return $this->site;
    }
}
