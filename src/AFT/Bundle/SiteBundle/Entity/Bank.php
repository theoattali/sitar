<?php

namespace AFT\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AFT\Bundle\SiteBundle\Repository\SiteRepository")
 */
class Bank
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="branch_code", type="string", length=10)
     * @Assert\NotBlank(
     *     message = "Le code banque ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "5",
     *     max = "5",
     *     exactMessage = "Le code banque doit être constitué de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $branchCode;

    /**
     * @ORM\Column(name="bank_name", type="string", length=50)
     * @Assert\NotBlank(
     *     message = "Le nom de banque ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "50",
     *     minMessage = "Le nom de banque doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le nom de banque doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $bankName;

    /**
     * @ORM\Column(name="sort_code", type="string", length=10)
     * @Assert\NotBlank(
     *     message = "Le code guichet ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "5",
     *     max = "5",
     *     exactMessage = "Le code guichet doit être constitué de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $sortCode;

    /**
     * @ORM\Column(name="account_number", type="string", length=20)
     * @Assert\NotBlank(
     *     message = "Le numéro de compte ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "11",
     *     max = "11",
     *     exactMessage = "Le numéro de compte doit être constitué de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $accountNumber;

    /**
     * @ORM\Column(name="rib_key", type="string", length=10)
     * @Assert\NotBlank(
     *     message = "La clef RIB ne peut être ignorée.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "2",
     *     max = "2",
     *     exactMessage = "La clef RIB doit être constituée de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $ribKey;

    /**
     * @ORM\Column(name="iban", type="string", length=35)
     * @Assert\NotBlank(
     *     message = "Le code IBAN ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Iban(
     *     message = "Le code IBAN n'est pas conforme.",
     *     groups = {"edit"}
     * )
     */
    private $iban;

    /**
     * @ORM\Column(name="tva_intracom_number", type="string", length=20, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "20",
     *     minMessage = "Le numéro TVA Intracom doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le numéro TVA Intracom doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $tvaIntracomNumber;

    /**
     * @ORM\OneToOne(targetEntity="Site", inversedBy="bank")
     */
    private $site;


    public function getId()
    {
        return $this->id;
    }

    public function setBranchCode($branchCode)
    {
        $this->branchCode = $branchCode;
    }

    public function getBranchCode()
    {
        return $this->branchCode;
    }

    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    public function getBankName()
    {
        return $this->bankName;
    }

    public function setSortCode($sortCode)
    {
        $this->sortCode = $sortCode;
    }

    public function getSortCode()
    {
        return $this->sortCode;
    }

    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    public function setRibKey($ribKey)
    {
        $this->ribKey = $ribKey;
    }

    public function getRibKey()
    {
        return $this->ribKey;
    }

    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    public function getIban()
    {
        return $this->iban;
    }

    public function setTvaIntracomNumber($tvaIntracomNumber = null)
    {
        $this->tvaIntracomNumber = $tvaIntracomNumber;
    }

    public function getTvaIntracomNumber()
    {
        return $this->tvaIntracomNumber;
    }

    public function setSite(Site $site)
    {
        $this->site = $site;
    }

    public function getSite()
    {
        return $this->site;
    }
}
