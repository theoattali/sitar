<?php

namespace AFT\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AFT\Bundle\SiteBundle\Repository\SiteRepository")
 */
class Property
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="adonix_code", type="string", length=25)
     * @Assert\NotBlank(
     *     message = "Le code adonix ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "5",
     *     max = "5",
     *     exactMessage = "Le code adonix doit être constitué de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $adonixCode;

    /**
     * @ORM\Column(name="territory_code", type="string", length=25)
     * @Assert\NotBlank(
     *     message = "Le code territoire ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "3",
     *     exactMessage = "Le code territoire doit être constitué de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $territoryCode;

    /**
     * @ORM\Column(name="selligent_code", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le code selligent doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code selligent doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $selligentCode;

    /**
     * @ORM\Column(name="aft_management_code", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le code gestion aft doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code gestion aft doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $aftManagementCode;

    /**
     * @ORM\Column(name="iftim_management_code", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le code gestion iftim doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code gestion iftim doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $iftimManagementCode;

    /**
     * @ORM\Column(name="aftral_management_code", type="string", length=25)
     * @Assert\NotBlank(
     *     message = "Le code gestion aftral ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le code gestion aftral doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code gestion aftral doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $aftralManagementCode;

    /**
     * @ORM\Column(name="transport_code", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le code transport doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code transport doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $transportCode;

    /**
     * @ORM\Column(name="logistic_code", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le code logistique doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code logistique doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $logisticCode;

    /**
     * @ORM\Column(name="background_color", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "7",
     *     max = "7",
     *     exactMessage = "La couleur de fond doit être constituée de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $backgroundColor;

    /**
     * @ORM\Column(name="text_color", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "7",
     *     max = "7",
     *     exactMessage = "La couleur de texte doit être constitué de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $textColor;

    /**
     * @ORM\Column(name="cqc_Number", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le numéro CQC doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le numéro CQC doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $cqcNumber;

    /**
     * @ORM\Column(name="cadr_Number", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le numéro CADR doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le numéro CADR doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $cadrNumber;

    /**
     * @ORM\Column(name="ad_code", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le code active directory doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le code active directory doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $adCode;

    /**
     * @ORM\Column(name="server_ip", type="string", length=25, nullable=true)
     * @Assert\Ip(
     *     message = "L'adresse Ip n'est pas conforme.",
     *     groups = {"edit"}
     * )
     */
    private $serverIp;

    /**
     * @ORM\Column(name="server_name", type="string", length=25, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "25",
     *     minMessage = "Le nom de serveur doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le nom de serveur doit être constitué de {{ limit }} caractères maximum.",
     *     groups = {"edit"}
     * )
     */
    private $serverName;

    /**
     * @ORM\Column(name="virtual_ndv", type="boolean")
     */
    private $virtualNdv;

    /**
     * @ORM\OneToOne(targetEntity="Site", inversedBy="property")
     */
    private $site;


    public function getId()
    {
        return $this->id;
    }

    public function setAdonixCode($adonixCode)
    {
        $this->adonixCode = $adonixCode;
    }

    public function getAdonixCode()
    {
        return $this->adonixCode;
    }

    public function setTerritoryCode($territoryCode)
    {
        $this->territoryCode = $territoryCode;
    }

    public function getTerritoryCode()
    {
        return $this->territoryCode;
    }

    public function setSelligentCode($selligentCode = null)
    {
        $this->selligentCode = $selligentCode;
    }

    public function getSelligentCode()
    {
        return $this->selligentCode;
    }

    public function setAftManagementCode($aftManagementCode = null)
    {
        $this->aftManagementCode = $aftManagementCode;
    }

    public function getAftManagementCode()
    {
        return $this->aftManagementCode;
    }

    public function setIftimManagementCode($iftimManagementCode = null)
    {
        $this->iftimManagementCode = $iftimManagementCode;
    }

    public function getIftimManagementCode()
    {
        return $this->iftimManagementCode;
    }

    public function setAftralManagementCode($aftralManagementCode)
    {
        $this->aftralManagementCode = $aftralManagementCode;
    }

    public function getAftralManagementCode()
    {
        return $this->aftralManagementCode;
    }

    public function setTransportCode($transportCode = null)
    {
        $this->transportCode = $transportCode;
    }

    public function getTransportCode()
    {
        return $this->transportCode;
    }

    public function setLogisticCode($logisticCode = null)
    {
        $this->logisticCode = $logisticCode;
    }

    public function getLogisticCode()
    {
        return $this->logisticCode;
    }

    public function setBackgroundColor($backgroundColor = null)
    {
        $this->backgroundColor = $backgroundColor;
    }

    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    public function setTextColor($textColor = null)
    {
        $this->textColor = $textColor;
    }

    public function getTextColor()
    {
        return $this->textColor;
    }

    public function setCqcNumber($cqcNumber = null)
    {
        $this->cqcNumber = $cqcNumber;
    }

    public function getCqcNumber()
    {
        return $this->cqcNumber;
    }

    public function setCadrNumber($cadrNumber = null)
    {
        $this->cadrNumber = $cadrNumber;
    }

    public function getCadrNumber()
    {
        return $this->cadrNumber;
    }

    public function setAdCode($adCode = null)
    {
        $this->adCode = $adCode;
    }

    public function getAdCode()
    {
        return $this->adCode;
    }

    public function setServerIp($serverIp = null)
    {
        $this->serverIp = $serverIp;
    }

    public function getServerIp()
    {
        return $this->serverIp;
    }

    public function setServerName($serverName = null)
    {
        $this->serverName = $serverName;
    }

    public function getServerName()
    {
        return $this->serverName;
    }

    public function setVirtualNdv($virtualNdv)
    {
        $this->virtualNdv = $virtualNdv;
    }

    public function getVirtualNdv()
    {
        return $this->virtualNdv;
    }

    public function setSite(Site $site)
    {
        $this->site = $site;
    }

    public function getSite()
    {
        return $this->site;
    }
}
