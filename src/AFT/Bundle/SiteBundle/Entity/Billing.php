<?php

namespace AFT\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AFT\Bundle\SiteBundle\Repository\SiteRepository")
 */
class Billing
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="year", type="integer")
     * @Assert\NotBlank(
     *     message = "L'année ne peut être ignorée.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "4",
     *     max = "4",
     *     exactMessage = "L'année doit être constituée de {{ limit }} caractères.",
     *     groups={"edit"}
     * )
     */
    private $year;

    /**
     * @ORM\Column(name="domain", type="string", length=10)
     * @Assert\NotBlank(
     *     message = "Le domaine ne peut être ignoré.",
     *     groups={"edit"}
     * )
     * @Assert\Length(
     *     min = "3",
     *     max = "10",
     *     minMessage = "Le domaine doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le domaine doit être constitué de {{ limit }} caractères maximum.",
     *     groups={"edit"}
     * )
     */
    private $domain;

    /**
     * @ORM\Column(name="team", type="string", length=10, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "10",
     *     minMessage = "L'équipe doit être constituée d'au moins {{ limit }} caractères.",
     *     maxMessage = "L'équipe doit être constituée de {{ limit }} caractères maximum.",
     *     groups={"edit"}
     * )
     */
    private $team;

    /**
     * @ORM\Column(name="team_id", type="bigint", nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     minMessage = "L'id équipe doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "L'id équipe doit être constitué de {{ limit }} caractères maximum.",
     *     groups={"edit"}
     * )
     */
    private $teamId;

    /**
     * @ORM\Column(name="mention", type="string", length=512, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "512",
     *     minMessage = "La mention doit être constituée d'au moins {{ limit }} caractères.",
     *     maxMessage = "La mention doit être constituée de {{ limit }} caractères maximum.",
     *     groups={"edit"}
     * )
     */
    private $mention;

    /**
     * @ORM\Column(name="mode", type="string", length=50, nullable=true)
     * @Assert\Length(
     *     min = "3",
     *     max = "50",
     *     minMessage = "Le mode doit être constitué d'au moins {{ limit }} caractères.",
     *     maxMessage = "Le mode doit être constitué de {{ limit }} caractères maximum.",
     *     groups={"edit"}
     * )
     */
    private $mode;

    /**
     * @ORM\OneToOne(targetEntity="Site", inversedBy="billing")
     */
    private $site;


    public function getId()
    {
        return $this->id;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setTeam($team = null)
    {
        $this->team = $team;
    }

    public function getTeam()
    {
        return $this->team;
    }

    public function setTeamId($teamId = null)
    {
        $this->teamId = $teamId;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setMention($mention = null)
    {
        $this->mention = $mention;
    }

    public function getMention()
    {
        return $this->mention;
    }

    public function setMode($mode = null)
    {
        $this->mode = $mode;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setSite(Site $site)
    {
        $this->site = $site;
    }

    public function getSite()
    {
        return $this->site;
    }
}
