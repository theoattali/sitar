<?php

namespace AFT\Bundle\SiteBundle\Tests\Controller;

use AFT\Bundle\SiteBundle\Entity\Site;
use AFT\Bundle\SecurityBundle\Test\SecurityWebTestCase;

class RestSearchControllerTest extends SecurityWebTestCase
{
    public function testGetByIdAction()
    {
        //Test avec un id invalide
        $crawler = $this->client->request('GET', '/api/site/0.json');
        $this->assertStatusCode(404);

        //Test avec un id valide
        $crawler = $this->client->request('GET', '/api/site/7.json');
        $this->assertStatusCode(200);

        $this->assertJson($this->client->getResponse()->getContent());

        $jsonExpected = '{"site":{"id":7,"code":"212221","corporateName":"AFTRAL AVIGNON","name":"AVIGNON","type":"Centre","addresses":[{"streetNumber":null,"streetLine1":"GARONOR","streetLine2":"BAT P","streetLine3":"BP 614","streetLine4":null,"zipCode":"93600","cityName":"AULNAY SOUS BOIS"},{"streetNumber":null,"streetLine1":"ZI GARONOR","streetLine2":"BATIMENT P","streetLine3":null,"streetLine4":null,"zipCode":"93600","cityName":"AULNAY SOUS BOIS"}],"parent":{"id":5,"code":"218440","name":"GARONOR","type":"Centre"},"property":{"adonixCode":"18440","territoryCode":"184","selligentCode":null,"aftManagementCode":"GA218440","iftimManagementCode":"GI218440","aftralManagementCode":"GF218440","transportCode":"287","logisticCode":"087","backgroundColor":null,"textColor":null,"cqcNumber":"71000016","cadrNumber":"78130185","adCode":"A28020","serverIp":null,"serverName":"A28020A00S001","virtualNdv":false}}}';
        $this->assertJsonStringEqualsJsonString($jsonExpected, $this->client->getResponse()->getContent());
    }

    public function testSearchSiteAction()
    {
        //Test recherche avec un seul résultat
        $crawler = $this->client->request('POST', '/api/site/search.json',
            array(
                'code' => '212221',
                'corporateName' => 'AFTRAL AVIGNON',
                'name' => 'AVIGNON',
                'type' => 'Centre',
                'limit' => 1,
            ),
            array(),
            array(
                'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            )
        );
        $this->assertStatusCode(200);

        $this->assertJson($this->client->getResponse()->getContent());

        $jsonExpected = '[{"id":7,"code":"212221","corporateName":"AFTRAL AVIGNON","name":"AVIGNON","type":"Centre","addresses":[{"streetNumber":null,"streetLine1":"GARONOR","streetLine2":"BAT P","streetLine3":"BP 614","streetLine4":null,"zipCode":"93600","cityName":"AULNAY SOUS BOIS"},{"streetNumber":null,"streetLine1":"ZI GARONOR","streetLine2":"BATIMENT P","streetLine3":null,"streetLine4":null,"zipCode":"93600","cityName":"AULNAY SOUS BOIS"}]}]';
        $this->assertJsonStringEqualsJsonString($jsonExpected, $this->client->getResponse()->getContent());

        //Test recherche avec plusieurs résultats
        $crawler = $this->client->request('POST', '/api/site/search.json',
            array(
                'corporateName' => 'AFTRAL F',
            ),
            array(),
            array(
                'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            )
        );
        $this->assertStatusCode(200);

        $jsonExpected = '[{"id":30,"code":"21322X","corporateName":"AFTRAL Formateurs d Entreprise","name":"Formateurs d Entreprise","type":"Centre","addresses":[{"streetNumber":null,"streetLine1":"ALLEE DE GASCOGNE","streetLine2":"BP 32","streetLine3":null,"streetLine4":null,"zipCode":"33370","cityName":"ARTIGUES PRES BORDEAUX"}]},{"id":20,"code":"222850","corporateName":"AFTRAL FORMATION DE FORMATEURS","name":"FORMATION DE FORMATEURS","type":"Centre","addresses":[{"streetNumber":null,"streetLine1":"340 RUE DE LA GARE","streetLine2":null,"streetLine3":null,"streetLine4":null,"zipCode":"74370","cityName":"PRINGY"}]},{"id":87,"code":"215600","corporateName":"AFTRAL FRANCHE-COMTE","name":"FRANCHE-COMTE","type":"R\u00e9gion","addresses":[{"streetNumber":null,"streetLine1":"RUE DU MASSIF CENTRAL ","streetLine2":null,"streetLine3":null,"streetLine4":null,"zipCode":"68490","cityName":"OTTMARSHEIM"}]}]';
        $this->assertJsonStringEqualsJsonString($jsonExpected, $this->client->getResponse()->getContent());

        //Test recherche avec un formulaire invalide
        $crawler = $this->client->request('POST', '/api/site/search.json',
            array(
                'qsdffdsq' => 'AFTRAL F',
            ),
            array(),
            array(
                'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            )
        );
        $this->assertStatusCode(400);

    }
}
