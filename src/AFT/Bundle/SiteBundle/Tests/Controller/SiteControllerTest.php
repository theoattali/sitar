<?php

namespace AFT\Bundle\SiteBundle\Tests\Controller;

use AFT\Bundle\SiteBundle\Entity\Site;
use AFT\Bundle\SecurityBundle\Test\SecurityWebTestCase;

class SiteControllerTest extends SecurityWebTestCase
{
    public function testSearchAction()
    {
        $this->login('site_user', 'site_user');

        $crawler = $this->client->request('GET', '/');
        $this->assertStatusCode(200);

        $this->assertCount(0, $crawler->filter('table tbody tr'));

        //Test avec aucun résultat
        $form = $crawler->selectButton('Rechercher')->form();
        $this->assertCount(6, $form->all());

        $crawler = $this->client->submit($form, array('aft_site_search[code]' => '000000'));
        $this->assertStatusCode(200);

        $this->assertCount(0, $crawler->filter('table tbody tr'));
        $this->assertContains(
            'Attention! Aucun résultat ne correspond à votre recherche.',
            $crawler->filter('.alert-warning')->text()
        );

        //Test avec plusieurs résultats
        $link = $crawler->selectButton('Rechercher')->form();
        $crawler = $this->client->submit($form, array('aft_site_search[code]' => '219'));
        $this->assertStatusCode(200);

        $this->assertCount(17, $crawler->filter('table tbody tr'));
    }

    public function testNewAction()
    {
        $this->login('site_user', 'site_user');

        //Test editer un site
        $crawler = $this->client->request('GET', '/new');
        $this->assertStatusCode(200);

        //Test ajouter des coordonnées bancaires
        $form = $crawler->selectButton('Enregistrer')->form();

        $form['aft_site[type]']->select('4');

        $crawler = $this->client->submit($form, array(
            'aft_site[corporateName]' => 'AFTRAL CFATL Picardie',
            'aft_site[name]' => 'Monchy-Saint-Eloi',
            'aft_site[code]' => '113210',
            'aft_site[beginingActivityAt]' => date('d/m/Y'),
            'aft_site[websiteUrl]' => 'http://www.aftral.com',
            'aft_site[mail]' => 'dsi@aftral.com',
        ));
        $this->assertStatusCode(302);

        $this->client->followRedirect();
        $this->assertStatusCode(200);
    }

    public function testShowAction()
    {
        $this->login('site_user', 'site_user');

        //Test avec un id invalide
        $crawler = $this->client->request('GET', '/0/show');
        $this->assertStatusCode(404);

        //Test recherche avec un seul résultat
        $crawler = $this->client->request('GET', '/');

        $form = $crawler->selectButton('Rechercher')->form();
        $crawler = $this->client->submit($form, array('aft_site_search[code]' => '212221'));

        $this->assertCount(1, $crawler->filter('table tbody tr'));

        $path = $crawler->filter('table tbody tr')->attr('data-path');
        $this->assertNotEmpty($path);

        $crawler = $this->client->request('GET', $path);
        $this->assertStatusCode(200);

        //Test avec un site complet
        $this->assertCount(6, $crawler->filter('.panel'));
        $this->assertCount(2, $crawler->filter('address'));
    }

    public function testLogAction()
    {
        $this->login('site_user', 'site_user');

        //Test avec un id invalide
        $crawler = $this->client->request('GET', '/0/log');
        $this->assertStatusCode(404);

        //Test avec un id valide
        $crawler = $this->client->request('GET', '/7/log');
        $this->assertStatusCode(200);

        $this->assertCount(8, $crawler->filter('#log-fields li'));
    }

    public function testEditAction()
    {
        $this->login('site_user', 'site_user');

        //Test avec un id invalide
        $crawler = $this->client->request('GET', '/0/edit');
        $this->assertStatusCode(404);

        //Test editer un site
        $crawler = $this->client->request('GET', '/2/show');

        $link = $crawler->selectLink('Editer')->link();
        $crawler = $this->client->click($link);
        $this->assertStatusCode(200);

        $form = $crawler->selectButton('Enregistrer')->form();
        $this->assertCount(19, $form->all());

        $this->client->submit($form, array('aft_site[beginingActivityAt]' => date('d/m/Y')));
        $this->assertStatusCode(302);

        $crawler = $this->client->followRedirect();
        $this->assertStatusCode(200);

        //Test ajouter deux adresses
        $link = $crawler->selectLink('Adresse')->link();
        $crawler = $this->client->click($link);
        $this->assertStatusCode(200);

        $form = $crawler->selectButton('Enregistrer')->form();
        $values = $form->getPhpValues();
        $values['aft_site_comp']['addresses'] = array(
            array(
                'streetLine1' => 'Rue de la république',
                'streetLine2' => 'BP 70091',
                'streetLine3' => 'Monchy Saint Eloi',
                'cityName' => 'Breuil le vert Cedex',
                'cityCode' => '60107',
                'zipCode' => '60603',
                'country' => 'FR',
                'phoneCode' => '+33',
                'phoneNumber' => '3-44-66-39-76',
            ),
            array(
                'streetLine1' => 'Rue de la république',
                'cityName' => 'Breuil le vert Cedex',
                'zipCode' => '60603',
                'country' => 'FR',
                'phoneCode' => '+33',
                'phoneNumber' => '3-44-66-39-76',
            )
        );
        $this->client->request($form->getMethod(), $form->getUri(), $values);
        $this->assertStatusCode(302);

        $this->client->followRedirect();
        $this->assertStatusCode(200);

        //Test supprimer une adresse
        $form = $crawler->selectButton('Enregistrer')->form();
        $values = $form->getPhpValues();
        $values['aft_site_comp']['addresses'] = array(
            array(
                'streetLine1' => 'Rue de la république',
                'streetLine2' => 'BP 70091',
                'streetLine3' => 'Monchy Saint Eloi',
                'cityName' => 'Breuil le vert Cedex',
                'cityCode' => '60107',
                'zipCode' => '60603',
                'country' => 'FR',
                'phoneCode' => '+33',
                'phoneNumber' => '3-44-66-39-76',
            )
        );

        $this->client->request($form->getMethod(), $form->getUri(), $values);
        $this->client->followRedirect();
    }

    public function testAddEditComponentAction()
    {
        $this->login('site_user', 'site_user');

        //Test editer un site
        $crawler = $this->client->request('GET', '/0/edit/bank');
        $this->assertStatusCode(404);

        //Test editer un site
        $crawler = $this->client->request('GET', '/2/show');

        //Test ajouter des coordonnées bancaires
        $link = $crawler->selectLink('Banque')->link();
        $crawler = $this->client->click($link);
        $this->assertStatusCode(200);

        $form = $crawler->selectButton('Enregistrer')->form();
        $crawler = $this->client->submit($form, array(
            'aft_site_comp[bank][bankName]' => 'CIC Paris Prony',
            'aft_site_comp[bank][branchCode]' => '30066',
            'aft_site_comp[bank][sortCode]' => '10141',
            'aft_site_comp[bank][accountNumber]' => '00010310201',
            'aft_site_comp[bank][ribKey]' => '27',
            'aft_site_comp[bank][iban]' => 'FR76 3006 6101 4100 0103 1020 127',
        ));
        $this->assertStatusCode(302);

        $this->client->followRedirect();
        $this->assertStatusCode(200);
    }

    public function testDisableAction()
    {
        $this->login('site_user', 'site_user');

        //Test editer un site
        $crawler = $this->client->request('GET', '/0/disable');
        $this->assertStatusCode(404);

        //Test editer un site
        $crawler = $this->client->request('GET', '/2/show');

        //Test ajouter des coordonnées bancaires
        $link = $crawler->selectLink('Clôturer le site')->link();
        $crawler = $this->client->click($link);
        $this->assertStatusCode(200);

        $form = $crawler->selectButton('Clôturer le site')->form();

        $form['aft_site_disable[successor]']->select('5');
        $crawler = $this->client->submit($form, array(
            'aft_site_disable[disabledAt]' => date('d/m/Y'),
        ));
    }
}
