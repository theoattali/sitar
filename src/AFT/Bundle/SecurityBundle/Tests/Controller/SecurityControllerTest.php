<?php

namespace AFT\Bundle\SecurityBundle\Tests\Controller;

use AFT\Bundle\SecurityBundle\Test\SecurityWebTestCase;

class SecurityControllerTest extends SecurityWebTestCase
{
    public function testLoginAction()
    {
        $this->client->request('GET', '/');

        $this->login('site_user', 'site_user');

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $this->assertSame('http://localhost/', $this->client->getResponse()->headers->get('Location'));
        $this->assertNotEmpty($this->getCollector('security')->getRoles());
    }

    public function testLoginActionWithBadPassword()
    {
        $this->login('site_user', 'u');

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $this->assertSame('http://localhost/login', $this->client->getResponse()->headers->get('Location'));

        $crawler = $this->client->followRedirect();

        $this->assertSame('Nom d\'utilisateur ou mot de passe incorrect', $crawler->filter('div > label')->text());
    }

    public function testLogoutAction()
    {
        $this->login('site_user', 'site_user');

        $this->assertNotEmpty($this->getCollector('security')->getRoles());

        $crawler = $this->client->request('GET', '/');

        $this->assertStatusCode(200);

        $link = $crawler->selectLink('Déconnexion')->link();
        $crawler = $this->client->click($link);

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $this->assertSame('http://localhost/login', $this->client->getResponse()->headers->get('Location'));

        $this->assertEmpty($this->getCollector('security')->getRoles());
    }
}
