<?php

namespace AFT\Bundle\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $roles = $this->get('security.token_storage')->getToken()->getRoles();
        if (!empty($roles)) {
            return $this->redirect($this->generateUrl('site_search'));
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        return $this->render('AFTSecurityBundle:Security:login.html.twig', array(
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
        ));
    }
}
