<?php

namespace AFT\Bundle\SecurityBundle\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityWebTestCase extends WebTestCase
{
    protected $client;

    public function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    protected function getCollector($name)
    {
        return $this->client->getProfile()->getCollector($name);
    }

    protected function assertStatusCode($statusCode)
    {
        $this->assertEquals($statusCode, $this->client->getResponse()->getStatusCode('Assert the status code'));
    }

    protected function login($login, $password)
    {
        $crawler = $this->client->request('GET', '/login');

        $buttonSubmit = $crawler->selectButton('submit');
        $crawler = $this->client->submit($buttonSubmit->form(), array(
            '_username' => $login,
            '_password' => $password,
        ));
    }
}
