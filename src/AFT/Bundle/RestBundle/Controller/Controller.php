<?php

namespace AFT\Bundle\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;

class Controller extends BaseController
{
    protected function json($data = null, $code = 200)
    {
        return new JsonResponse($data, $code);
    }

    protected function badRequest($msg = null)
    {
        return $this->json($msg, 400);
    }

    protected function notFound($msg = null)
    {
        return $this->json($msg, 404);
    }

    protected function prepareErrorMessage($form)
    {
        $errorMsg = array();
        if ($form->getExtraData()) {
            $errorMsg['extraData']['message'] = 'Cet échange ne doit pas contenir de champs supplémentaires.';
            foreach ($form->getExtraData() as $fields => $value) {
                $errorMsg['extraData']['fields'][] = $fields;
            }
        } elseif (false === $form->isSubmitted()) {
            $errorMsg['notSubmitted']['message'] = 'Aucun critère de recherche attendu n\'a été renseigné.';
        }

        foreach ($form->getErrors() as $error) {
            $errorMsg[]['message'] = $error->getMessage();
        }

        return $errorMsg;
    }
}
