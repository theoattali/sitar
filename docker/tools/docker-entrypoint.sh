#!/bin/bash

DIRECTORIES=(var/cache var/logs var/sessions)

USERNAME=tmp_user

# create group and user to run command with same UID and GID
groupadd --force --gid=${GROUP_ID} tmp_group
useradd --uid=${USER_ID} --gid=${GROUP_ID} --home-dir /var/tmp/tmp_user ${USERNAME}

# set rights directory
setfacl -R  -m u:${USERNAME}:rwX ${DIRECTORIES[@]}
setfacl -dR -m u:${USERNAME}:rwX ${DIRECTORIES[@]}

# run input command
sudo --user ${USERNAME} $@
