#!/bin/bash

DIRECTORIES=(var/cache var/logs var/sessions)

# create group and user to run command with same UID and GID
setfacl -R  -m u:www-data:rwX ${DIRECTORIES[@]}
setfacl -dR -m u:www-data:rwX ${DIRECTORIES[@]}

# run input command
$@
