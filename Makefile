ifndef SYMFONY_ENV
	export SYMFONY_ENV:=dev
endif

all: composer resetdb

cc: cacheclear
cacheclear:
	./bin/console cache:clear
	rm -f ./var/logs/$(SYMFONY_ENV).log

resetdb: dropdb createdb fixtures

createdb:
	./bin/console doctrine:database:create
	./bin/console doctrine:schema:create

dropdb:
	-./bin/console doctrine:database:drop --force

fixtures:
	./bin/console doctrine:fixtures:load --no-interaction


phpunit:
	./bin/phpunit -c app


test: SYMFONY_ENV:=test
test: cacheclear resetdb phpunit


composer:
	composer install --prefer-dist
